import React from 'react'
import Tiles from 'grommet/components/Tiles'
import Tile from 'grommet/components/Tile'
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Header from 'grommet/components/Header'
import Paragraph from 'grommet/components/Paragraph'
import Image from 'grommet/components/Image'


class CategoryStep extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  handlePrevStep = () => {
    this.props.prevHandler()
  }

  handleSelection = (selected, field) => {
    var obj = {};
    switch (field) {
      case 'category':
        obj = { 'category': selected };
        break;
      case 'specie':
        obj = { 'specie': selected };
        break;
      case 'sex':
        obj = { 'sex': selected };
        break;
    }

    this.props.nextHandler(obj);
  }

  render() {

    return (
      <Box size="large">
        <Heading tag="h4" margin="small" align="center" strong={true}>{this.props.header}</Heading>
        <Tiles selectable={true}>
          {this.props.options.map((option, key) => {
            return (
              <Tile justify="start" responsive={false} direction="row" wide={true} key={key} onClick={() => this.handleSelection(option.val, option.field)}>
                <Box pad="medium" alignSelf="center" >
                  <Image src={option.image} size="thumb" />
                </Box>
                <Box pad="small" alignSelf="center" flex={true}>
                  <Heading tag="h4" margin="none" strong={true}>{option.header}</Heading>
                  <Paragraph margin="none">{option.description}</Paragraph>
                </Box>
              </Tile>
            )
          })}
        </Tiles>
      </Box>
    )
  }
}

export default CategoryStep