import HomeLayout from '../components/HomeLayout'
import Link from 'next/link'

//Grommet Components
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Paragraph from 'grommet/components/Paragraph'
import Button from 'grommet/components/Button'
import Image from 'grommet/components/Image'

const Congrats = (props) => (
  <HomeLayout>
    <Box full={true} justify="center" align="center" pad="small">
      <Image src="/static/buscandohuellas_logo_azul.png" size="medium" />
      <Heading tag="h1" strong={true}>¡Felicitaciones!</Heading>
      <Paragraph align="center">
        En breve nos vamos a poner en contacto contigo para conocer como fue tu experiencia
        y poder contar tu historia en nuestras redes sociales.
      </Paragraph>
      <Link href="/userpanel">
        <Button primary={true} label="Volver a la plataforma" />
      </Link>
    </Box>
  </HomeLayout>
)

export default Congrats