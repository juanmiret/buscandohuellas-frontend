import React from 'react'
import Router from 'next/router'
import Link from 'next/link'
import { User } from '../services/API'


//Grommet Components
import Button from 'grommet/components/Button'
import Sidebar from 'grommet/components/Sidebar'
import Header from 'grommet/components/Header'
import Heading from 'grommet/components/Heading'
import Menu from 'grommet/components/Menu'
import Box from 'grommet/components/Box'
import Anchor from 'grommet/components/Anchor'
import Footer from 'grommet/components/Footer'
const UserIcon = require('grommet/components/icons/base/User')
import Close from 'grommet/components/icons/base/Close'
import Add from 'grommet/components/icons/base/Add'
import Search from 'grommet/components/icons/base/Search'
import Up from 'grommet/components/icons/base/Up'
import Favorite from 'grommet/components/icons/base/Favorite'
import Attraction from 'grommet/components/icons/base/Attraction'
import Image from 'grommet/components/Image'
import LoginIcon from 'grommet/components/icons/base/Login'
import Map from 'grommet/components/icons/base/Map'
import Alert from 'grommet/components/icons/base/Alert'

export default class UserSidebar extends React.Component {
  constructor(props) {
    super(props)
  }

  renderCloseIcon = () => {
    if (this.props.small) {
      return (
        <Anchor icon={<Close />} onClick={this.props.closeSidebar} />
      )
    }
  }

  activeClass = (route) => {
    if (this.props.pathname === route) {
      return "active"
    }
  }

  render() {
    return (
      <Sidebar
        justify="between"
        colorIndex='neutral-1'
        size="small"
        full={!this.props.small}>
        <Header pad='small'
          justify='between'>
          <Link href='/'>
            <Image src='/static/bh_logo.png' size="small" style={{ cursor: 'pointer' }} />
          </Link>
          {this.renderCloseIcon()}
        </Header>
        {
          this.props.user &&
          <Box align="center" justify="center" margin="small" direction="row" responsive={false}>
            <Image size="thumb" src={this.props.user.profile_picture} className="profileImage" style={{ marginRight: '8px' }} />
            <Heading tag="h4" margin="none">{this.props.user.name && this.props.user.name}</Heading>
          </Box>
        }
        <Box align='start' pad="none" direction="column">
          <Menu primary>
            <Anchor className={this.activeClass('/userpanel')} label="Mi Perfil" icon={<UserIcon />} onClick={() => {
              Router.push('/userpanel')
              this.props.closeSidebar()
            }} />
            <Anchor className={this.activeClass('/usersearches')} label="Mis Búsquedas" icon={<Search />} onClick={() => {
              Router.push('/usersearches')
              this.props.closeSidebar()
            }} />
            <Anchor className={this.activeClass('/addsearch')} label="Nueva Búsqueda" icon={<Add />} onClick={() => {
              Router.push('/addsearch')
              this.props.closeSidebar()
            }} />
            <Anchor className={this.activeClass('/adoptions')} label="Adopciones" icon={<Favorite />} onClick={() => {
              Router.push('/adoptions')
              this.props.closeSidebar()
            }} />
            <Anchor className={this.activeClass('/emergencies')} label="Emergencias" icon={<Alert />} onClick={() => {
              Router.push('/emergencies')
              this.props.closeSidebar()
            }} />
            <Anchor className={this.activeClass('/vets')} label="Veterinarias" icon={<Attraction />} onClick={() => {
              Router.push('/vets')
              this.props.closeSidebar()
            }} />
          </Menu>
        </Box>
        <Footer pad="small" alignSelf="end" justify="center" direction="column">
          {
            this.props.user ?
              <Anchor label="Cerrar Sesión" onClick={() => {
                User.logout()
                this.props.closeSidebar()
              }} />
              :
              <Link prefetch href='/login'>
                <Button primary={true} icon={<LoginIcon />} label="Iniciar sesión" color="light-1" className="heroLoginButton" />
              </Link>
          }
          <Anchor label="Contactanos" href="mailto:contacto@buscandohuellas.com" target="_blank" />
        </Footer>
      </Sidebar>
    )
  }
}