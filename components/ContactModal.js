import Modal from '../components/Modal'

//Grommet Comps
import Box from 'grommet/components/Box'
import Image from 'grommet/components/Image'
import Heading from 'grommet/components/Heading'
import Anchor from 'grommet/components/Anchor'
import AccessVolumeControl from 'grommet/components/icons/base/AccessVolumeControl'
import Mail from 'grommet/components/icons/base/Mail'

const ContactModal = (props) => (
  <Modal onDismiss={props.onDismiss}>
    <Box pad="large" align="center">
      <Heading tag="h3">Datos de Contacto:</Heading>
      {props.user && <Image size="thumb" src={props.user.profile_picture} className="profileImage" />}
      {props.user && <Heading tag="h4">{props.user.name}</Heading>}
      <Anchor icon={<AccessVolumeControl />} label={props.doc.phone} />
      <div style={{ height: '15px' }}></div>
      <Anchor icon={<Mail />} label={props.user.email} href={`mailto:${props.user.email}`} target="_blank" />
      <div style={{ height: '15px' }}></div>
      {props.user.facebook_id &&
        <Anchor
          label="Ver perfil de Facebook"
          href={`https://facebook.com/${props.user.facebook_id}`}
          target="_blank" />
      }
      {props.user.google_id &&
        <Anchor
          label="Ver perfil de Google"
          href={`https://plus.google.com/${props.user.google_id}`}
          target="_blank" />
      }
    </Box>
  </Modal>
)

export default ContactModal