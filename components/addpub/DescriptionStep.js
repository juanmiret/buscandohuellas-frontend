import React, { Component } from 'react'
import Joi from 'joi-browser'

//Grommet Components
import Form from 'grommet/components/Form'
import FormField from 'grommet/components/FormField'
import TextInput from 'grommet/components/TextInput'
import Button from 'grommet/components/Button'
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Spinning from 'grommet/components/icons/Spinning'
import LinkNext from 'grommet/components/icons/base/LinkNext'

import DatePicker from 'react-datepicker'
import moment from 'moment'

moment.locale('es')


class DescriptionStep extends Component {
  constructor(props) {
    super(props)
    this.state = {
      date: { value: moment(), error: null },
      pet_name: { value: '', error: null },
      title: { value: '', error: null },
      description: { value: '', error: null },
      phone: { value: '', error: null }
    }
  }

  handleChange = ({ target }) => {
    this.setState({
      [ target.name ]: {
        value: target.value,
        error: null
      }
    })
  }

  handleDateChange = (date) => {
    this.setState({
      date: { value: date, error: null }
    })
  }

  handlePrevStep = () => {
    this.props.prevHandler()
  }

  submit = () => {
    let { pet_name, title, description, phone, email, date } = this.state
    let schema = Joi.object().keys({
      pet_name: Joi.string().allow(''),
      title: Joi.string().required(),
      description: Joi.string().allow(''),
      phone: Joi.string().required(),
      date: Joi.string().allow('')
    })
    let formFields = {
      pet_name: pet_name.value,
      title: title.value,
      description: description.value,
      phone: phone.value,
      date: date.value.format('L')
    }
    let result = Joi.validate(formFields, schema, {
      abortEarly: false,
      language: {
        any: {
          empty: '!!No puede estar vacío'
        }
      }
    })

    if (result.error) {
      console.log(result)
      result.error.details.map((error) => {
        this.setState({
          [ error.path ]: {
            ...this.state[ error.path ],
            error: error.message
          }
        })
      })
    } else {
      this.setState({ loading: true })
      this.props.nextHandler(formFields)
    }
  }

  getButtonLabel = () => {
    switch (this.props.category) {
      case 'emergency':
        return 'PUBLICAR EMERGENCIA'
        break
      case 'adoption':
        return 'PUBLICAR ADOPCIÓN'
        break
      default:
        return 'REALIZAR BÚSQUEDA'
    }
  }

  render() {
    const { pet_name, title, description, phone, date } = this.state

    return (
      <Box responsive={false} full="horizontal" align="center">
        <Heading tag="h4" margin="small" align="center" strong={true}>Completa los datos:</Heading>
        <Form size="large" pad="medium" className="descriptionForm">
          {
            [ 'lost', 'found' ].includes(this.props.category) &&
            <FormField label="Fecha que se perdió/encontró" style={{ overflow: 'visible' }}>
              <DatePicker
                selected={date.value}
                onChange={this.handleDateChange} />
            </FormField>
          }
          {
            [ 'lost', 'adoption' ].includes(this.props.category) &&
            <FormField label="Nombre de la Mascota" error={pet_name.error}>
              <TextInput name='pet_name' value={pet_name.value} onDOMChange={this.handleChange} />
            </FormField>
          }
          <FormField label="Titulo de la Publiación" error={title.error}>
            <TextInput name='title' value={title.value} onDOMChange={this.handleChange} />
          </FormField>
          <FormField label="Descripción / Mensaje" error={description.error}>
            <textarea rows="4" name='description' value={description.value} onChange={this.handleChange} />
          </FormField>
          <FormField label="Teléfono de Contacto" error={phone.error}>
            <TextInput name='phone' value={phone.value} onDOMChange={this.handleChange} />
          </FormField>
        </Form>
        <Button
          icon={
            this.state.loading ? <Spinning className="whiteSpinning" /> : <LinkNext />
          }
          label={this.getButtonLabel()}
          primary={true}
          onClick={this.submit} />
      </Box>
    )
  }
}

export default DescriptionStep