import Box from 'grommet/components/Box'
import Link from 'next/link'
import Anchor from 'grommet/components/Anchor'
import Alert from 'grommet/components/icons/base/Alert'
import Favorite from 'grommet/components/icons/base/Favorite'
import Add from 'grommet/components/icons/base/Add'


const ProfileLinks = (props) => (
  <Box align="center">
    <Link href="/addsearch">
      <Anchor label="Publicar nueva búsqueda" icon={<Add />} />
    </Link>
    <div style={{ height: '15px' }}></div>
    <Link href="/adoptions">
      <Anchor label="Adopciones" icon={<Favorite />} />
    </Link>
    <div style={{ height: '15px' }}></div>
    <Link href="/emergencies">
      <Anchor label="Emergencias" icon={<Alert />} />
    </Link>
  </Box>
)

export default ProfileLinks