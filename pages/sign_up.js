import React from 'react'
import Joi from 'joi-browser'
import HomeLayout from '../components/HomeLayout'
import { User } from '../services/API'
import redirect from '../lib/redirect'
import Link from 'next/link'

//Grommet Components
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Paragraph from 'grommet/components/Paragraph'
import Image from 'grommet/components/Image'
import Spinning from 'grommet/components/icons/Spinning'
import Form from 'grommet/components/Form'
import FormField from 'grommet/components/FormField'
import TextInput from 'grommet/components/TextInput'
import Button from 'grommet/components/Button'

export default class SignUp extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: { value: '', error: null },
      email: { value: '', error: null },
      password: { value: '', error: null },
      password_confirmation: { value: '', error: null },
      loading: false,
      loginError: null,
      accountCreated: false
    }
  }

  static async getInitialProps(ctx) {
    const user = await User.getCurrentUser(ctx)
    const redirectPath = ctx.query.redirect
    if (user) {
      redirect(ctx, '/userpanel')
      return {}
    } else {
      return {
        redirectPath: redirectPath
      }
    }
  }

  handleChange = ({ target }) => {
    this.setState({
      [ target.name ]: {
        value: target.value,
        error: null
      }
    })
  }

  submit = () => {
    let { name, email, password, password_confirmation } = this.state
    let schema = Joi.object().keys({
      name: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().min(6).required(),
      password_confirmation: Joi.string().required().valid(Joi.ref('password'))
    })
    let formFields = {
      name: name.value,
      email: email.value,
      password: password.value,
      password_confirmation: password_confirmation.value
    }
    let result = Joi.validate(formFields, schema, {
      abortEarly: false,
      language: {
        any: {
          empty: '!!No puede estar vacío',
          allowOnly: '!!No coincide'
        },
        string: {
          email: '!!Ingresa un email válido',
          min: '!!Minimo 6 caracteres.'
        }
      }
    })

    if (result.error) {
      result.error.details.map((error) => {
        this.setState({
          [ error.path ]: {
            ...this.state[ error.path ],
            error: error.message
          }
        })
      })
    } else {
      this.startSignUp(formFields)
    }
  }

  async startSignUp(fields) {
    this.setState({ loading: true, loginError: null })
    const response = await User.signUpWithEmail(fields)
    if (response.error) {
      this.setState({
        loading: false,
        loginError: response.message
      })
    } else if (response.created) {
      this.setState({
        loading: false,
        loginError: null,
        accountCreated: true
      })
    }
  }

  render() {
    let { name, email, password, password_confirmation } = this.state
    return (
      <HomeLayout>
        <Box full={true} justify="center" align="center" pad="small">
          <Image src="/static/buscandohuellas_logo_azul.png" size="small" />
          <Heading tag="h3" strong align="center" > Registrate: </Heading>
          {this.state.loading && <Spinning size="medium" />}
          {this.state.loginError && <Paragraph>*{this.state.loginError}</Paragraph>}
          {
            this.state.accountCreated ?
              <Paragraph align="center">
                Tu cuenta fue creada con éxito, para poder ingresar, es necesario que confirmes tu email.
              Te enviamos el boton de confirmación a tu casilla de correo, por favor revísalo.
            </Paragraph>
              :
              <Box>
                <Form compact>
                  <FormField label="Nombre Completo" error={name.error}>
                    <TextInput name="name" value={name.value} onDOMChange={this.handleChange} />
                  </FormField>
                  <FormField label="Email" error={email.error}>
                    <TextInput name="email" value={email.value} onDOMChange={this.handleChange} />
                  </FormField>
                  <FormField label="Contraseña" error={password.error}>
                    <TextInput type="password" name="password" value={password.value} onDOMChange={this.handleChange} />
                  </FormField>
                  <FormField label="Repetir Contraseña" error={password_confirmation.error}>
                    <TextInput type="password" name="password_confirmation" value={password_confirmation.value} onDOMChange={this.handleChange} />
                  </FormField>
                </Form>
                <Button primary label="Registrarme" onClick={this.submit} />
              </Box>
          }
          <Paragraph size="small" align="center">
            Al crear una cuenta aceptas nuestros <Link href="/terms">Terminos y Condiciones</Link> de uso.
          </Paragraph>
        </Box>
      </HomeLayout>
    )
  }

}