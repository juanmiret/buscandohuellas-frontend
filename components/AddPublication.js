import React from 'react';
import { categoryOptions, specieOptions, sexOptions } from './addpub/OptionsBorrador'

//Grommet Comps
import Button from 'grommet/components/Button'
import Animate from 'grommet/components/Animate';
import Box from 'grommet/components/Box'
import LinkPrevious from 'grommet/components/icons/base/LinkPrevious'

// App Comps
import SelectionStep from './addpub/SelectionStep'
import LocationStep from './addpub/LocationStep'
import UploadStep from './addpub/UploadStep'
import DescriptionStep from './addpub/DescriptionStep'

class AddPublication extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      finished: false,
      stepIndex: props.category ? 1 : 0,
      search_data: {
        pet_name: '',
        title: '',
        category: props.category || null,
        specie: null,
        sex: null,
        address: '',
        lat: null,
        lng: null,
        gallery: {},
        description: '',
        email: '',
        phone: ''
      }
    }
  }

  handleNext = (state) => {
    const { stepIndex } = this.state;
    this.setState({
      stepIndex: stepIndex + 1,
      search_data: {
        ...this.state.search_data,
        ...state
      }
    });
  };

  handleSubmit = (formData) => {
    let searchData = {
      ...this.state.search_data,
      ...formData
    }
    this.props.onSubmit(searchData);
  }

  handlePrev = () => {
    const { stepIndex } = this.state;
    this.setState({
      stepIndex: stepIndex - 1
    })
  };

  getStepContent() {
    switch (this.state.stepIndex) {
      case 0:
        return (
          <SelectionStep
            key="1"
            options={categoryOptions}
            fieldName="category"
            header="Selecciona una Categoría:"
            nextHandler={this.handleNext}
            prevHandler={this.handlePrev} />
        );
      case 1:
        return (
          <SelectionStep
            key="2"
            options={specieOptions}
            fieldName="specie"
            header="Selecciona una Especie:"
            nextHandler={this.handleNext}
            prevHandler={this.handlePrev} />
        );
      case 2:
        return (
          <SelectionStep
            key="3"
            options={sexOptions}
            fieldName="sex"
            header="Selecciona el Sexo:"
            nextHandler={this.handleNext}
            prevHandler={this.handlePrev} />
        )
      case 3:
        return (
          <LocationStep key="4" nextHandler={this.handleNext} prevHandler={this.handlePrev} runJoyride={this.props.runJoyride} />
        )
      case 4:
        return (
          <UploadStep key="5" nextHandler={this.handleNext} prevHandler={this.handlePrev} />
        );
      case 5:
        return (
          <DescriptionStep key="6" nextHandler={this.handleSubmit} prevHandler={this.handlePrev} category={this.state.search_data.category} />
        );
      default:
        return '';
    }
  }

  render() {
    return (
      <Box pad="small" align="center" justify="center" full={true} className="addPubWrapper">

        {this.getStepContent()}

        <Button icon={<LinkPrevious />} className={this.state.stepIndex > (this.props.category ? 1 : 0) && this.state.stepIndex < 5 ? 'prevStepButton' : 'hidden'} onClick={this.handlePrev} />

      </Box>
    )
  }

}

export default AddPublication;