const searchDisplayName = (pet_name, title) => {
  let displayName = pet_name ? `${pet_name.toUpperCase()} - ${title}` : title
  return displayName
}

export default searchDisplayName