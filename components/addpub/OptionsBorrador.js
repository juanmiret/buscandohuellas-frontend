export const categoryOptions = [
  {
    header: 'Perdidos',
    image: '/static/marker_lost.png',
    description: 'Selecciona esta opción si perdiste a una mascota',
    val: 'lost',
    field: 'category'
  },
  {
    header: 'Encontrados',
    image: '/static/marker_found.png',
    description: 'Selecciona esta opción si encontraste a una mascota perdida y buscas a su familia',
    val: 'found',
    field: 'category'
  }
]

export const specieOptions = [
  {
    header: 'Perro',
    image: '/static/perro.png',
    val: 'dog',
    field: 'specie'
  },
  {
    header: 'Gato',
    image: '/static/gato.png',
    val: 'cat',
    field: 'specie'
  },
  {
    header: 'Otro',
    image: '/static/burro.png',
    val: 'other',
    field: 'specie'
  }
]

export const sexOptions = [
  {
    header: 'Macho',
    image: '/static/male.png',
    val: 'male',
    field: 'sex'
  },
  {
    header: 'Hembra',
    image: '/static/female.png',
    val: 'female',
    field: 'sex'
  }
]