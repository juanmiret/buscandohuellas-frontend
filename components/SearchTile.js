import React, { Component } from 'react'
import { CSSTransition, transit } from "react-css-transition";
import LazyLoad from 'react-lazyload';


//Grommet Components
import Tile from 'grommet/components/Tile'
import Card from 'grommet/components/Card'
import Header from 'grommet/components/Header'
import Heading from 'grommet/components/Heading'
import Box from 'grommet/components/Box'
import Image from 'grommet/components/Image'
import Menu from 'grommet/components/Menu'
import Anchor from 'grommet/components/Anchor'


// Icons
import Actions from 'grommet/components/icons/base/Actions'
import Close from 'grommet/components/icons/base/Close'

//Helpers
import { getSpecie, getSex, getCategory } from '../helpers/displayTraductions'
import renderThumbnail from '../helpers/renderThumbnail'
import searchDisplayName from '../helpers/searchDisplayName'

import Link from 'next/link'

class SearchTile extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    if (this.props.index == 0) {
      this.props.runJoyride()
    }
  }

  render() {
    let { search, dist } = this.props
    return (
      <LazyLoad once unmountIfInvisible={true} height={350}>
        <CSSTransition
          defaultStyle={{ opacity: 1 }}
          enterStyle={{ opacity: transit(0, 500, "ease-in-out") }}
          leaveStyle={{ opacity: transit(1, 500, "ease-in-out") }}
          activeStyle={{ display: 'none' }}
          active={search.hidden ? true : false}
        >
          <Tile
            key={search.id}
            colorIndex="light-1"
            margin="small"
            className="searchTile">
            <div className={`searchTileTop ${search.category}`}>{getCategory(search.category)}</div>
            <Card
              alignSelf="stretch"
              textSize="small"
              thumbnail={renderThumbnail(search.gallery)}
            >
              {
                this.props.withMeta &&
                <div className="searchMeta">
                  A {dist && dist.toFixed(1)} kms {getSpecie(search.specie)} {getSex(search.sex)}
                </div>
              }

              <Heading tag="h4" strong={true}>
                {searchDisplayName(search.pet_name, search.title)}
              </Heading>

              {this.props.children}

            </Card>
          </Tile>
        </CSSTransition>
      </LazyLoad>
    )
  }
}

export default SearchTile