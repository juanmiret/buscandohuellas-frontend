import React, { Component } from 'react'
import Head from 'next/head'
import UserSidebar from './UserSidebar'
import Router from 'next/router'
import GaWrapper from '../components/GaWrapper'

import App from 'grommet/components/App'
import Split from 'grommet/components/Split'
import Header from 'grommet/components/Header'
import Anchor from 'grommet/components/Anchor'
import Image from 'grommet/components/Image'
import Responsive from 'grommet/utils/Responsive'
import Box from 'grommet/components/Box'
import Menu from 'grommet/components/icons/base/Menu'

import { ToastContainer } from 'react-toastify';


class Layout extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    this._responsive = Responsive.start(this._onResponsive);
    Router.prefetch('/userpanel')
    Router.prefetch('/usersearches')
    Router.prefetch('/addsearch')
    Router.prefetch('/myzone')
    Router.prefetch('/vets')
    Router.prefetch('/emergencies')
    Router.prefetch('/adoptions')
    Router.prefetch('/setvolunteer')
  }

  componentWillUnmount() {
    this._responsive.stop();
  }

  _onResponsive = (small) => {
    this.setState({ small });
  }

  openSidebar = () => {
    this.setState({
      priority: 'left'
    })
  }

  closeSidebar = () => {
    this.setState({
      priority: 'right'
    })
  }

  renderMobileHeader() {
    if (this.state.small) {
      return (
        <Header colorIndex="neutral-1" pad="small" justify="between" fixed={true}>
          <Image src="/static/bh_logo.png" size="small" />
          <Box>
            <Anchor icon={<Menu />} onClick={this.openSidebar} />
          </Box>
        </Header>
      )
    }
  }

  render() {
    return (
      <div>
        <Head>
          <title>{this.props.title + " - Buscando Huellas"}</title>
          <meta charSet='utf-8' />
        </Head>
        <App centered={false} className="appContainer">
          <Split flex="right" priority={this.state.priority || 'right'}>
            <UserSidebar user={this.props.user} closeSidebar={this.closeSidebar} small={this.state.small} pathname={this.props.pathname} />
            {this.renderMobileHeader()}
            {this.props.children}
          </Split>
          <ToastContainer
            position="bottom-right"
            hideProgressBar={true}
            autoClose={4000}
            closeButton={false}
            style={{ zIndex: '999999' }} />
        </App>
      </div>
    )
  }
}

export default GaWrapper(Layout)