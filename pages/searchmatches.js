import React, { Component } from 'react'
import nextCookies from '../lib/nextCookies'
import { User, Searches } from '../services/API'
import redirect from '../lib/redirect'

import Router from 'next/router'

//Grommet Components
import Button from 'grommet/components/Button'
import Tiles from 'grommet/components/Tiles'
import Tile from 'grommet/components/Tile'
import Header from 'grommet/components/Header'
import Paragraph from 'grommet/components/Paragraph'
import Box from 'grommet/components/Box'
import Image from 'grommet/components/Image'
import Anchor from 'grommet/components/Anchor'
import Heading from 'grommet/components/Heading'
import Close from 'grommet/components/icons/base/Close'
import Spinning from 'grommet/components/icons/Spinning'
import Article from 'grommet/components/Article'
import Section from 'grommet/components/Section'
import Columns from 'grommet/components/Columns'
import ListPlaceholder from 'grommet-addons/components/ListPlaceholder'

//App Components
import Layout from '../components/Layout'
import SearchTile from '../components/SearchTile'
import Modal from '../components/Modal'
import UserSidebar from '../components/UserSidebar'
import SearchHeader from '../components/SearchHeader'
import SearchModal from '../components/SearchModal'
import FoundModal from '../components/FoundModal'

import Joyride from 'react-joyride';



export default class SearchMatches extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      searches: [],
      searchModal: null,
      foundModal: null,
      foundModalCat: null,
      runJoyride: false,
    }
  }

  static async getInitialProps(ctx) {
    const searchDetails = await Searches.getSearchDetails(ctx.query.search_id)
    const user = await User.getCurrentUser(ctx)
    if (!user) {
      redirect(ctx, '/login?redirect=/usersearches')
    } else {
      return {
        user: user,
        userSearch: searchDetails
      }
    }
  }

  async discardSearch(search_id, index) {
    const res = await Searches.discardSearch(this.props.userSearch.id, search_id, this.props.user)
    if (res.ok) {
      let searchesArray = this.state.searches
      searchesArray[ index ].doc.hidden = true
      this.setState({
        searches: searchesArray
      })
    }
  }

  async setFoundSearch(search_id) {
    let res = await Searches.setFoundSearch(search_id)
    if (res.ok) {
      Router.push('/congrats')
    }
  }

  componentDidMount() {
    Searches.getSearchMatches(this.props.userSearch.id, (result) => {
      this.setState({
        searches: result,
        loading: false
      })
    })
  }

  dismissModal = () => {
    this.setState({ searchModal: null })
  }

  renderModal = () => {
    if (this.state.searchModal) {
      return (
        <SearchModal {...this.state.searchModal}
          discardSearch={this.discardSearch.bind(this)}
          searchModalIndex={this.state.searchModalIndex}
          onDismiss={() => this.dismissModal()} />
      )
    }
  }

  joyrideCallback = (data) => {
    if (data.type === 'finished') {
      User.setTutorialDone()
    }
  }

  runJoyride = () => {
    setTimeout(() => {
      this.setState({
        runJoyride: true
      })
    }, 600)
  }

  openFoundModal = (search_id, category) => {
    this.setState({ foundModal: search_id, foundModalCat: category })
  }

  closeFoundModal = () => {
    this.setState({ foundModal: null })
  }

  confirmFoundModal = () => {
    this.setFoundSearch(this.state.foundModal)
  }

  joyrideSteps = [
    {
      title: 'Tutorial',
      text: 'Descarta las búsquedas que no coincidan.',
      selector: '.discard0'
    },
    {
      title: 'Tutorial',
      text: 'Haz click en Ver Detalles para ver la descripcion de la búsqueda y contactar a quien la publicó.',
      selector: '.details0'
    },
    {
      title: 'Tutorial',
      text: 'Haz click aquí si pudiste reencontrarte con tu mascota o encontrar a la familia de una mascota perdida.',
      selector: '.foundButton'
    }
  ]

  render() {
    return (
      <div>
        <Joyride
          ref={c => (this.joyride = c)}
          callback={this.joyrideCallback}
          resizeDebounce={true}
          locale={{
            back: (<span>Atrás</span>),
            close: (<span>Cerrar</span>),
            last: (<span>Listo</span>),
            next: (<span>Siguiente</span>),
            skip: (<span>Omitir</span>),
          }}
          run={this.state.runJoyride && !User.getTutorialDone()}
          autoStart={true}
          steps={this.joyrideSteps}
          type='continuous'
          scrollToSteps={false}
        />
        <Layout user={this.props.user} pathname={this.props.url.pathname} title="Resultados de Búsqueda">
          {this.renderModal()}
          <Article full={true} colorIndex="light-2">
            <SearchHeader search={this.props.userSearch} openFoundModal={this.openFoundModal} />
            {
              this.state.foundModal &&
              <FoundModal onConfirm={this.confirmFoundModal} onDismiss={this.closeFoundModal} category={this.state.foundModalCat} />
            }
            {
              this.state.loading ?
                <Section flex="grow" direction="column" justify="center" align="center">
                  <Spinning size="large" />
                </Section>
                :
                <Section pad="small">
                  <Heading align="center" tag="h3">Resultados que coinciden con tu búsqueda:</Heading>
                  <Tiles flush={false} justify="start" fill={true}>
                    <ListPlaceholder
                      className="listPlaceholder"
                      emptyMessage="No hay resultados para tu búsqueda en este momento. Te enviaremos una notificacion por email cuando alguien publique una búsqueda en tu zona que coincida."
                      unfilteredTotal={this.state.searches.length}
                      filteredTotal={this.state.searches.length} />
                    {this.state.searches.map((search, index) => {
                      return (
                        <SearchTile key={index} index={index} search={search.doc} dist={search.dist} runJoyride={this.runJoyride} withMeta>
                          <Header size='small' align="center" justify="between">
                            <Anchor className={`discard${index}`} icon={<Close />} label="Descartar" onClick={() => this.discardSearch(search.doc.id, index)} />
                            <Anchor className={`details${index}`} primary={true} label="Ver Detalles" onClick={() => {
                              this.setState({ searchModal: search, searchModalIndex: index })
                            }} />
                          </Header>
                        </SearchTile>
                      )
                    })}
                  </Tiles>
                </Section>
            }
          </Article>
        </Layout>
      </div>
    )
  }
}