const getSpecie = (specie) => {
  switch (specie) {
    case 'dog':
      return '/ Especie: Perro'
      break
    case 'cat':
      return '/ Especie: Gato'
      break
    case 'other':
      return '/ Especie: Otros'
      break
    default:
      return specie
  }
}

const getSex = (sex) => {
  switch (sex) {
    case 'male':
      return '/ Sexo: Macho'
      break
    case 'female':
      return '/ Sexo: Hembra'
      break
    default:
      return sex
  }
}

const getCategory = (category) => {
  switch (category) {
    case 'vet':
      return 'Embajador Oficial'
      break
    case 'lost':
      return 'Perdido'
      break
    case 'found':
      return 'Encontrado'
      break
    case 'adoption':
      return 'En Adopción'
      break
    case 'emergency':
      return 'Emergencia'
      break
    default:
      return category
  }
}


export { getSpecie, getSex, getCategory }