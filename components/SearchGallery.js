import Gallery from 'react-grid-gallery'

const cdn_url = "https://ucarecdn.com"

const SearchGallery = (props) => (
  <div>
    <iframe
      src={`${cdn_url}/${props.gallery.uuid}/gallery/-/nav/thumbs/-/loop/true/-/allowfullscreen/native/-/thumbwidth/100/`}
      className="gallery-iframe"
      allowFullScreen="true"
      frameBorder="0">
    </iframe>
    <style jsx>{`
      iframe {
        width: 100%;
        height: 80vh;
        overflow: hidden;
      }
      @media only screen and (max-width: 767px) {
        iframe {
          height: 50vh;
        }
      }
    `}
    </style>
  </div>
)

export default SearchGallery