import React, { Component } from 'react'
import { User, Searches } from '../services/API'
import redirect from '../lib/redirect'
import Router from 'next/router'



//App Components
import AddPublication from '../components/AddPublication'
import Layout from '../components/Layout'

import Joyride from 'react-joyride'
import { toast } from 'react-toastify'


export default class AddSearch extends Component {
  constructor(props) {
    super(props)
    this.state = {
      runJoyride: false
    }
  }

  static async getInitialProps(ctx) {
    const user = await User.getCurrentUser(ctx)
    const category = ctx.query.category
    if (!user) {
      redirect(ctx, '/login?redirect=/addsearch')
    } else if (!user.email_confirmed) {
      redirect(ctx, '/userpanel?message=Por favor confirma tu email para continuar utilizando la plataforma.')
    }
    return {
      user: user,
      category: category
    }
  }

  createSearch = (search_data) => {
    Searches.createSearch(search_data, (result) => {
      if (result.id) {
        Router.push('/usersearches')
      }
      //Router.push(`/searchmatches?search_id=${result.id}`)
    })
  }

  runJoyride = () => {
    setTimeout(() => {
      this.setState({
        runJoyride: true
      })
    }, 600)
  }

  joyrideSteps = [
    {
      title: 'Ingresa una ubicación válida',
      text: `
        Ingresa una ubicación y selecciona una opción de la lista.
        Asegúrate de escribir la dirección sin esquinas. También puedes 
        escribir una ciudad y luego arrastrar el marcador hasta la
        ubicación deseada. Asegúrate de que la ubicación sea correcta antes de
        continuar.
      `,
      selector: '.geosuggest'
    }
  ];

  render() {
    return (
      <div>
        <Joyride
          ref={c => (this.joyride = c)}
          resizeDebounce={true}
          locale={{
            back: (<span>Atras</span>),
            close: (<span>Cerrar</span>),
            last: (<span>Listo</span>),
            next: (<span>Siguiente</span>),
            skip: (<span>Omitir</span>),
          }}
          run={this.state.runJoyride}
          autoStart={true}
          steps={this.joyrideSteps}
          type='continuous'
          allowClicksThruHole={true}
        />
        <Layout user={this.props.user} pathname={this.props.url.pathname} title="Nueva Búsqueda">
          <AddPublication
            category={this.props.category}
            onSubmit={this.createSearch}
            runJoyride={this.runJoyride} />
        </Layout>
      </div>
    )
  }
}