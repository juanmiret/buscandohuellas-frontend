import React from 'react'
import Link from 'next/link'
import GaWrapper from '../components/GaWrapper'
import ReactGA from 'react-ga'

//Grommet Components
import Box from 'grommet/components/Box'
import Button from 'grommet/components/Button'
import Split from 'grommet/components/Split'
import Anchor from 'grommet/components/Anchor'
import Close from 'grommet/components/icons/base/Close'

//App components
import SearchLabels from '../components/SearchLabels'
import ContactModal from '../components/ContactModal'
import SearchGallery from '../components/SearchGallery'
import SearchDetails from '../components/SearchDetails'
import ContactButton from '../components/ContactButton'

export default class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showContactModal: false
    }
  }

  componentDidMount() {
    ReactGA.pageview('/search_modal' + '?search_id=' + this.props.doc.id);
    document.getElementsByTagName('body')[ 0 ].classList.add('noscroll')
  }

  componentWillUnmount() {
    document.getElementsByTagName('body')[ 0 ].classList.remove('noscroll')
  }

  dismiss(e) {
    if (this._shim === e.target ||
      this._photoWrap === e.target) {
      if (this.props.onDismiss) {
        this.props.onDismiss()
      }
    }
  }

  showContactModal = () => {
    this.setState({
      showContactModal: true
    })
  }

  closeContactModal = () => {
    this.setState({
      showContactModal: false
    })
  }

  render() {
    return (
      <div ref={el => (this._shim = el)} className='sm-shim' onClick={(e) => this.dismiss(e)}>
        <div ref={el => (this._photoWrap = el)} className='sm-content'>
          <Button className="closeSearchModal" icon={<Close />} onClick={this.props.onDismiss} />
          <Split
            fixed={false}
            className="searchModalSplit"
            flex="both"
            showOnResponsive="both">
            <Box pad="small" justify="center">
              <SearchGallery gallery={this.props.doc.gallery} />
            </Box>
            <Box pad="small">
              <SearchDetails {...this.props.doc} />
            </Box>
          </Split>
        </div>
        <Box justify="center" align="center" direction="row" pad="small" colorIndex="light-2" className="searchButtons">
          {
            this.props.noDiscard ? null :
              this.props.doc.category && this.props.doc.category != 'adoption' &&
              <Button className="discardButton" label="Descartar" primary={true} icon={<Close />} onClick={() => {
                this.props.discardSearch(this.props.doc.id, this.props.searchModalIndex)
                this.props.onDismiss()
              }} />
          }
          <ContactButton onClick={this.showContactModal} />
        </Box>
        {
          this.state.showContactModal ?
            <ContactModal onDismiss={this.closeContactModal} user={this.props.user} doc={this.props.doc} /> : null
        }
      </div>
    )
  }
}