import React from 'react'
import Router from 'next/router'
import { User, Adoptions } from '../services/API'

//App Comps
import Layout from '../components/Layout'
import ProfileHeader from '../components/ProfileHeader'

//Grommet Comps
import Button from 'grommet/components/Button'
import Article from 'grommet/components/Article'
import Section from 'grommet/components/Section'
import Quote from 'grommet/components/Quote';
import Paragraph from 'grommet/components/Paragraph'


export default class AdoptionsPage extends React.Component {
  constructor(props) {
    super(props)
  }

  static async getInitialProps(ctx) {
    const user = await User.getCurrentUser(ctx)
    return {
      user: user
    }
  }

  render() {
    return (
      <Layout user={this.props.user} pathname={this.props.url.pathname} title="Adopciones">
        <Article>
          <ProfileHeader title="Adopciones" />
          <Section align="center" justify="center">
            <Quote credit="Thorn Jones">
              <Paragraph>
                Los perros tienen una forma de encontrar a quienes los necesitamos, llenando un vacío que ni siquiera sabíamos que teníamos.
              </Paragraph>
            </Quote>
            <Paragraph>¿Quieres adoptar una mascota?</Paragraph>
            <Button label="Ver mascotas en adopción" primary={true} onClick={() => Router.push('/searchadoptions')} />
            <Paragraph>¿Quieres dar una mascota en adopción?</Paragraph>
            <Button label="Dar en adopción" primary={true} onClick={() => Router.push('/addsearch?category=adoption')} />
          </Section>
        </Article>
      </Layout>
    )
  }

}