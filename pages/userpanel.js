import React, { Component } from 'react'
import Router from 'next/router'

import redirect from '../lib/redirect'
import nextCookies from '../lib/nextCookies'
import { User } from '../services/API'
import Joi from 'joi-browser'
import { toast } from 'react-toastify';

//Grommet Components
import Button from 'grommet/components/Button'
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Image from 'grommet/components/Image'
import Anchor from 'grommet/components/Anchor'
import Split from 'grommet/components/Split'
import Paragraph from 'grommet/components/Paragraph'
import FormField from 'grommet/components/FormField'
import TextInput from 'grommet/components/TextInput'
import Form from 'grommet/components/Form'
import Spinning from 'grommet/components/icons/Spinning'
import LinkNext from 'grommet/components/icons/base/LinkNext'

//App Components
import UserSidebar from '../components/UserSidebar'
import ProfileHeader from '../components/ProfileHeader'
import Layout from '../components/Layout'
import ProfileInfo from '../components/ProfileInfo'
import ProfileLinks from '../components/ProfileLinks'
import AddVolunteerText from '../components/AddVolunteerText'
import VolunteerData from '../components/VolunteerData'
import Modal from '../components/Modal'

class UserPanel extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modifyEmailModal: false,
      modifyEmailLoading: false,
      updateEmailError: null,
      modifyEmailSuccessful: false,
      message: props.message,
      email: { value: props.user.email, error: null }
    }
  }

  static async getInitialProps(ctx) {
    const user = await User.getCurrentUser(ctx)
    const message = ctx.query.message
    if (!user) {
      redirect(ctx, '/login')
      return {}
    } else {
      return {
        user: user,
        message: message
      }
    }
  }

  async unsetVolunteer() {
    let res = await User.unsetVolunteer()
    if (res.ok) {
      Router.push('/userpanel')
    }
  }

  openModifyEmail = () => {
    this.setState({
      modifyEmailModal: true
    })
  }

  handleChange = ({ target }) => {
    this.setState({
      [ target.name ]: {
        value: target.value,
        error: null
      }
    })
  }

  submitEmailChange = () => {
    let { email } = this.state
    let schema = Joi.object().keys({
      email: Joi.string().email().required()
    })
    let formFields = {
      email: email.value
    }
    let result = Joi.validate(formFields, schema, {
      language: {
        string: {
          email: '!!Ingresa un email válido',
        }
      }
    })

    if (result.error) {
      result.error.details.map((error) => {
        this.setState({
          [ error.path ]: {
            ...this.state[ error.path ],
            error: error.message
          }
        })
      })
    } else {
      this.startEmailUpdate(formFields.email)
    }
  }

  async startEmailUpdate(email) {
    this.setState({ modifyEmailLoading: true, updateEmailError: null })
    const response = await User.updateEmail(email)
    if (response.error) {
      this.setState({ modifyEmailLoading: false, updateEmailError: response.message })
    } else if (response.updated) {
      this.setState({ modifyEmailSuccessful: true, modifyEmailLoading: false })
    }
  }

  async resendConfirmation() {
    const response = await User.resendEmailConfirmation(this.props.user.email)
    if (response.success) {
      toast.success("Email de Confirmacion reenviado. Revisa tu casilla de correo.", {
        position: toast.POSITION.BOTTOM_CENTER
      })
    } else {
      toast.warn("Hubo un problema. Por favor intenta de nuevo mas tarde", {
        position: toast.POSITION.BOTTOM_CENTER
      })
    }

  }

  render() {
    let { user } = this.props
    return (
      <Layout user={user} pathname={this.props.url.pathname} title="Mi Panel">
        <ProfileHeader title="Mi Perfil" />
        {
          this.state.message &&
          <Modal onDismiss={() => this.setState({ message: null })}>
            <Box align="center" justify="center" pad="large">
              <Paragraph align="center">
                {this.state.message}
              </Paragraph>
            </Box>
          </Modal>
        }
        <Split
          fixed={false}
          flex="right"
          showOnResponsive="both">
          <Box align="center" justify="center" size="medium">
            <Box pad="medium" margin="small" colorIndex="light-1">
              <ProfileInfo {...user} openModifyEmail={this.openModifyEmail} resendConfirmation={this.resendConfirmation.bind(this)} />
              {
                this.state.modifyEmailModal &&
                <Modal onDismiss={() => document.location.href = '/userpanel'}>
                  <Box pad="large" align="center">
                    {
                      this.state.modifyEmailSuccessful ?
                        <Paragraph align="center">Tu email fue modificado, te hemos enviado un mail para confirmar tu nuevo correo.
                        Por favor revisa tu casilla y confirmalo para poder continuar utilizando la plataforma.
                      </Paragraph> :
                        <div>
                          <Form compact>
                            <FormField label="Email" error={this.state.email.error || this.state.updateEmailError}>
                              <TextInput name="email" value={this.state.email.value} onDOMChange={this.handleChange} />
                            </FormField>
                          </Form>
                          <div style={{ height: '20px' }}></div>
                          <Button primary label="Cambiar mi email" onClick={this.submitEmailChange} icon={this.state.modifyEmailLoading ? <Spinning className="whiteSpinning" /> : <LinkNext />} />
                        </div>
                    }
                  </Box>
                </Modal>
              }
            </Box>
            <Box pad="medium" margin="small" colorIndex="light-1">
              <ProfileLinks />
            </Box>
          </Box>
          <Box pad="medium" margin={{ vertical: 'small' }} align="center" justify="center" colorIndex="light-1">
            {
              (user.isVolunteer && user.volunteerData) ? <VolunteerData {...user.volunteerData} unsetVolunteer={this.unsetVolunteer.bind(this)} /> : <AddVolunteerText />
            }
          </Box>
        </Split>
      </Layout>
    )
  }
}

export default UserPanel