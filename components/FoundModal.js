import Modal from '../components/Modal'

//Grommet Comps
import Box from 'grommet/components/Box'
import Image from 'grommet/components/Image'
import Heading from 'grommet/components/Heading'
import Button from 'grommet/components/Button'
import Paragraph from 'grommet/components/Paragraph'
import Spinning from 'grommet/components/icons/Spinning'

const renderTitle = (category) => {
  switch (category) {
    case 'lost':
    case 'found':
      return 'Reencuentro:'
      break
    case 'adoption':
      return 'Adopcion:'
      break
    case 'emergency':
      return 'Emergencia resuelta:'
      break
  }
}

const renderText = (category) => {
  switch (category) {
    case 'lost':
    case 'found':
      return `
        Haz click en el siguiente botón si pudiste reencontrarte
        con tu mascota perdida o si encontraste a la familia de 
        una mascota perdida.
      `
      break
    case 'adoption':
      return `
        Haz click en el siguiente botón si pudiste dar en adopción con éxito.
      `
      break
    case 'emergency':
      return `Haz click en el siguiente boton si la emergencia fue resuelta con éxito.`
      break
  }
}

const renderButtonText = (category) => {
  switch (category) {
    case 'lost':
    case 'found':
      return "¡Se dio el reencuentro!"
      break
    case 'adoption':
      return "¡Se adoptó!"
      break
    case 'emergency':
      return "¡Emergencia resuelta!"
      break
  }
}

const FoundModal = (props) => (
  <Modal onDismiss={props.onDismiss}>
    <Box pad="large" align="center">
      <Heading tag="h3">{renderTitle(props.category)}</Heading>
      <Paragraph align="center">
        {renderText(props.category)}
      </Paragraph>
      {
        props.loading ? <Spinning /> :
          <Button label={renderButtonText(props.category)} className="contactButton" primary={true} onClick={props.onConfirm} />
      }
    </Box>
  </Modal>
)

export default FoundModal