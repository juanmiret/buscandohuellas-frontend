import canUseDOM from "can-use-dom";
import React from 'react'

//Map Comps
import { withGoogleMap, GoogleMap, Marker, Circle } from "react-google-maps";
import Geosuggest from 'react-geosuggest'


//Grommet Comps
import Spinning from 'grommet/components/icons/Spinning'
import LinkNext from 'grommet/components/icons/base/LinkNext'
import Button from 'grommet/components/Button'
import Box from 'grommet/components/Box'
import NumberInput from 'grommet/components/NumberInput'
import FormField from 'grommet/components/FormField'

const geolocation = (
  canUseDOM && navigator.geolocation ?
    navigator.geolocation :
    ({
      getCurrentPosition(success, failure) {
        failure(`Your browser doesn't support geolocation.`);
      },
    })
);

const LocationStepGoogleMap = withGoogleMap(props => (
  <GoogleMap
    defaultZoom={props.withRadius ? 13 : 15}
    center={props.center}
  >
    {
      props.center && (
        <Marker
          position={props.center}
          draggable={true}
          onDragEnd={props.onMarkerDragEnd}
        />
      )
    }
    {
      props.center && (
        props.withRadius && (
          <Circle
            center={props.center}
            radius={props.radius * 1000}
          />
        )
      )
    }
  </GoogleMap>
));


export default class LocationStep extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      center: null,
      radius: 5,
      content: null,
      address: '',
      loading: true
    };
    this.isUnmounted = false;
  }

  componentDidMount() {
    this.geocoder = new google.maps.Geocoder
    if (this.props.volunteerData) {
      this.setState({
        address: this.props.volunteerData.address,
        radius: this.props.volunteerData.radius,
        center: { lat: this.props.volunteerData.lat, lng: this.props.volunteerData.lng },
        loading: false
      })
      this.props.runJoyride()
    } else {
      geolocation.getCurrentPosition((position) => {
        if (this.isUnmounted) {
          return;
        }
        let center = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        }
        this.geocoder.geocode({ location: center }, (results, status) => {
          if (status === 'OK') {
            if (results[ 0 ]) {
              this.setState({ address: results[ 0 ].formatted_address })
            }
          }
        })
        this.setState({
          center: center,
          loading: false,
          content: `Location found using HTML5.`,
        });
        this.props.runJoyride()
      }, (reason) => {
        if (this.isUnmounted) {
          return;
        }
        this.setState({
          center: null,
          loading: false
        })
        this.props.runJoyride()
      });
    }
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  onDragEnd = (e) => {
    let position = {
      lat: e.latLng.lat(),
      lng: e.latLng.lng()
    }
    this.geocoder.geocode({ location: position }, (results, status) => {
      if (status === 'OK') {
        if (results[ 0 ]) {
          this.setState({ address: results[ 0 ].formatted_address })
        }
      }
    })
    this.setState({
      center: position
    })
  }

  onSuggestSelect = (suggest) => {
    this.setState({
      center: suggest.location,
      address: suggest.description
    })
  }

  handleNextStep = () => {
    if (!this.state.center) {
      this.setState({
        showError: true
      });
    }
    else {
      this.setState({
        showError: false
      });
      let location = {
        'address': this.state.address,
        lat: this.state.center.lat,
        lng: this.state.center.lng
      }
      if (this.props.withRadius) {
        location.radius = this.state.radius
      }
      this.props.nextHandler(location);
    }
  }

  changeRadius = (e) => {
    this.setState({
      radius: e.target.value
    })
  }

  render() {
    return (
      <Box flex="grow" justify="start" alignSelf="stretch" pad="small">
        {
          this.state.loading ?
            <Box align="center">
              <Spinning size="large" />
            </Box>
            :
            <div>
              <Geosuggest
                ref={el => this._geoSuggest = el}
                initialValue={this.state.address}
                placeholder="Ingresa una dirección y selecciona una opcion de la lista..."
                onSuggestSelect={this.onSuggestSelect}
              />
              {
                this.props.withRadius && (
                  <FormField label="Ajusta el radio que quieres abarcar" help={`${this.state.radius} Kms`}>
                    <input type="range" value={this.state.radius} name="radius" onChange={this.changeRadius} min={0.1} max={20} step={0.1} />
                  </FormField>
                )
              }
              <LocationStepGoogleMap
                containerElement={
                  <div style={{ height: `60vh`, width: '100%' }} />
                }
                mapElement={
                  <div style={{ height: `60vh`, width: '100%' }} />
                }
                center={this.state.center}
                withRadius={this.props.withRadius}
                radius={this.state.radius}
                content={this.state.content}
                onMarkerDragEnd={this.onDragEnd}
              />

            </div>
        }
        <Button icon={<LinkNext />} label="SIGUIENTE" primary={true} className="nextStepButton" onClick={this.handleNextStep} />
      </Box>

    );
  }
}