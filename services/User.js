class User {

  /**
   * Check if a user is authenticated - check if a token is saved in Local Storage
   *
   * @returns {boolean}
   */
  static isAuthenticated() {
    return window.localStorage.getItem('user') !== null;
  }

  /**
   * Deauthenticate a user. Remove a token from Local Storage.
   *
   */
  static logout() {
    window.localStorage.removeItem('user');
    window.location = '/'
  }

  /**
   * Get a token value.
   *
   * @returns {string}
   */

  static getUser() {
    return JSON.parse(window.localStorage.getItem('user'));
  }

  static getProfileImage() {
    var user = JSON.parse(window.localStorage.getItem('user'));
  }

  static getUserID() {
    var user = JSON.parse(window.localStorage.getItem('user'));
    return user.id;
  }

  static getUserToken() {
    var user = JSON.parse(window.localStorage.getItem('user'));
    return user.token;
  }

}

export default User;