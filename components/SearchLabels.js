import { getSpecie, getSex, getCategory } from '../helpers/displayTraductions'

const Label = (props) => {
  return (
    <span className={`label ${props.color}`}>{props.children}</span>
  )
}

const SearchLabels = (props) => {
  let { category, specie, sex } = props.search

  const categoryLabel = () => {
    switch (category) {
      case 'lost':
        return (
          <Label color="red">Perdido</Label>
        )
      case 'found':
        return (
          <Label color="yellow">Encontrado</Label>
        )
      case 'adoption':
        return (
          <Label color="blue">En Adopción</Label>
        )
      case 'emergency':
        return (
          <Label color="red">Emergencia</Label>
        )
    }
  }

  return (
    <div className="searchLabels">
      {categoryLabel()} {getSpecie(specie)} {getSex(sex)}
    </div>
  )
}

export default SearchLabels