import HomeLayout from '../components/HomeLayout'

import Heading from 'grommet/components/Heading'
import Paragraph from 'grommet/components/Paragraph'
import Box from 'grommet/components/Box'

const Terms = () => (
  <HomeLayout>
    <Box align="center" justify="center">

      <Heading tag="h3">Terminos y Condiciones de Uso</Heading>
      <Paragraph>
        1.El servicio de publicación de cualquier tipo de aviso, incluidos los de venta y/o promoción y/o publicidad de servicios y productos es ofrecido por ROCABELL S.R.L. en su carácter de responsable de la operación y explotación del sitio http://www.buscandohuellas.com (de aquí en más el “Sitio”), a los anunciantes y/o usuarios que accedan y se registren en el Sitio a fines de proceder con una o más publicaciones, con la condición de que acepten sin ninguna objeción todos y cada uno de los Términos y Condiciones de uso que se describen a continuación. Asimismo, debido a que ciertos contenidos que puedan ser accedidos a través del Sitio podrán estar alcanzados por normas específicas que reglamenten y complementen a los presentes, se recomienda a los Usuarios tomar conocimiento específico de ellas.
  </Paragraph>
      <Heading tag="h4">2. ALCANCE DE LOS TÉRMINOS Y CONDICIONES</Heading>
      <Paragraph>
        Fuera de lo estipulado en la última parte del apartado precedente, los presentes Términos y condiciones y las normas que los complementan sólo serán aplicables a los servicios y contenidos prestados y/o accesibles directamente en el Sitio y no a aquellos a los que los Usuarios puedan acceder a través de un hipervínculo (link), una barra co-branded, y/o cualquier otra herramienta de navegación ubicada en el Sitio que los lleve a navegar un recurso diferente.
La probable aparición de dichos links en el Sitio no implica de modo alguno la asunción de garantía por parte de ROCABELL S.R.L. sobre los productos, servicios o programas contenidos en ninguna página vinculada al Sitio por tales links, y en consecuencia, deslinda toda responsabilidad por el contenido de las mismas. En atención a ello, la utilización de los links para navegar hacia cualquier otra página queda al exclusivo criterio de responsabilidad y riesgo de los Usuarios.
</Paragraph>
      <Heading tag="h4">3. INGRESO Y PUBLICACIÓN</Heading>
      <Paragraph>

        A fin de publicar un aviso a través del Sitio, el Usuario deberá registrarse e ingresar de manera correcta siguiendo el procedimiento indicado para los fines, ademas de todo los datos, así como el texto, fotos y demás información del aviso que pretende publicar.
Buscando Huellas podrá realizar un control del mismo procediendo, entre otros criterios, a la revisión de su contenido e incluso a su remoción del Sitio si su contenido no se correspondiere con estos Términos y Condiciones, o al sentido de la plataforma, en cuyo caso el Usuario perderá todo derecho a reclamar, incluyendo si correspondiesen las sumas por él abonadas, sin derecho a reembolso de ninguna naturaleza.
El Servicio es pago en aquellos casos en que el usuario opte por un plan que otorgue beneficios en comparación al aviso gratuito, por lo que el Usuario se obliga a pagar los precios establecidos por Buscando Huellas, los que podrán ser cambiados por éste a su solo criterio.
Los distintos tipos de avisos vigentes y sus respectivos valores se encuentran disponibles en el proceso de publicación o en el sitio de manera clara y especifica, y el usuario declara haber tomado debido conocimiento y conformidad  con ellos .
</Paragraph>
      <Heading tag="h4">4. PUBLICACIÓN EN EL SITIO WEB</Heading>
      <Paragraph>
        4.1. Plazos de Publicación:
Cumplidos los requisitos del apartado anterior, Buscando Huellas publicará los avisos en el Sitio por 30 (treinta) días, con la posibilidad de re publicar el mismo aviso por el misma cantidad de tiempo.
4.2. Modificación y/o baja  de los avisos:
Los avisos podrán ser modificados o dado de baja  por los propios usuarios a través del administrador de avisos, al que se accede con el usuario y contraseña con el que se registraron o con el link de acceso que recibieron luego de publicar su aviso sin registrarse.
</Paragraph>
      <Heading tag="h4">5. CONDUCTA DE LOS USUARIOS</Heading>
      <Paragraph>
        Queda expresamente prohibido a “Los Usuarios” utilizar el Servicio para:
(i) publicar material ilegal, difamatorio, obsceno, pornográfico, racista, discriminatorio, agraviante, injurioso o que afecte la privacidad de las personas;
(ii)- publicar fotografías, propias o de terceros, cuya imagen sea obscena, inmoral o contraria a las buenas costumbres;
(iii) - publicar material mediante la falsificación de su identidad;
(iv) publicar material en infracción a la ley, violar cualquier legislación aplicable local, federal, nacional o internacional;
(v)- publicar o crear una base de datos personales de terceros para cualquier fin, comercial o delictivo.
(vi) publicar material con el fin de persuadir, engañar, y ulteriormente delinquir abusando de la confianza de otros usuarios y/o  incurriendo en cualquier tipo penal previsto por la legislación argentina.
(vii) publicar imágenes, fotografías, publicaciones, ideas, marcas, patentes, inventos, que no sean de su propiedad o que pertenezcan a terceras personas sean estas físicas o jurídicas o que estén protegidas por leyes de propiedad intelectual nacionales o internacionales.
El incumplimiento por parte de los Usuarios de cualquiera de las condiciones precedentes, implicará de inmediato la no publicación o baja del aviso en la plataforma.
</Paragraph>
      <Heading tag="h4">6. CALIDAD DE LOS SERVICIOS Y PRODUCTOS PROMOCIONADOS</Heading>
      <Paragraph>
        ROCABELL S.R.L. no manifiesta ni garantiza de modo alguno y por ende no asume responsabilidad de ninguna especie respecto a  publicaciones falsas por parte de los usuarios, en consecuencia, sugiere firmemente Buscando Huellas que la información brindada por La Plataforma respecto de las publicaciones, sea objeto de una investigación independiente y propia de quien esté interesado en la misma, no asumiendo ningún tipo de responsabilidad por la incorrección de la información, su desactualización o falsedad.
ROCABELL S.R.L. no asume ninguna obligación respecto del Usuario y/o los visitantes del “Sitio” en general y se limita tan sólo a publicar en el Sitio en forma similar a aquella en que lo haría una guía telefónica los datos de los Usuarios que han solicitado tal publicación y en la forma en que tales datos han sido proporcionados por tales Usuarios.

ROCABELL S.R.L. no resulta propietario de los productos y servicios que se publicitan, y por ende no garantiza en forma alguna dichos productos y servicios, ya sea respecto de su calidad, condiciones de entrega, prestación, precio, modalidades de pago, estado de conservación, como respecto de ningún otro aspecto, ni garantiza a los Usuarios y/o visitantes en general respecto de la existencia, crédito, capacidad, solvencia material y moral ni sobre ningún otro aspecto de los Usuarios proveedores de tales productos y servicios.
En todos aquellos que ROCABELL S.R.L. sea el distribuidor o proveedor directo del producto o servicio que se ofrece sera responsable con el alcance que indica la ley por las publicaciones realizadas, en aquellos casos en los que no es productor, fabricante, importador, distribuidor, proveedor ni vendedor de los productos o servicios que se ofrecen, el contrato se realizara fuera de la esfera de participación de ROCABELL S.R.L. y sin su intervención. Por tal virtud, ROCABELL S.R.L. no otorga garantía de evicción ni por vicios ocultos o aparentes de los bienes publicados, ni se responsabiliza de ningún daño o perjuicio que pudiera sufrir el usuario o los visitantes del “Sitio” en los términos de la ley 24.240, su decreto reglamentario y modificatorias.
</Paragraph>
      <Heading tag="h4">7. ESPACIO ASIGNADO EN EL SERVICIO</Heading>
      <Paragraph>
        El usuario reconoce que Buscando Huellas puede poner límites respecto al uso del Servicio, con inclusión, entre otras cosas, de la cantidad máxima de días que el Contenido será retenido por el Servicio, la cantidad y el tamaño máximo de los anuncios, los mensajes de email, o de todo otro Contenido que se puede transmitir o almacenar a través del Servicio, y la frecuencia con la cual el Usuario puede acceder al Servicio. El Usuario conviene en que Buscando Huellas no es responsable de la eliminación o el fracaso en el almacenamiento de aquel Contenido que se conserva o que se transmite a través del Servicio. El Usuario entiende que Buscando Huellas se reserva el derecho para, en todo momento, modificar o interrumpir el Servicio (o una parte de este) con o sin notificación, y que Buscando Huellas no será responsable ante el Usuario ni ante un tercero por la modificación, la suspensión o la interrupción del Servicio .
El Usuario conviene en que Buscando Huellas, a su entera y sola discreción, tiene el derecho (pero no la obligación) de eliminar o desactivar la cuenta del Usuario, de bloquear su dirección de email o IP, o bien de cualquier otra forma cancelar el acceso o el uso del Servicio (o parte de este) por parte del Usuario, en forma inmediata y sin notificación previa. Asimismo, Buscando Huellas podrá quitar o descartar Contenido dentro el Servicio, por cualquier motivo, con inclusión, no restrictiva, del momento en que Buscando Huellas considere que el Usuario actuó en forma contraria a la letra o el espíritu de las Condiciones. Sumado a ello, el Usuario conviene en que Buscando Huellas no será responsable ante el Usuario mismo ni ante un tercero por la cancelación del acceso del Usuario al Servicio. Además, el Usuario conviene en no intentar usar el Servicio luego de la cancelación por parte de Buscando Huellas.
</Paragraph>
      <Heading tag="h4">8. BASE DE DATOS</Heading>
      <Paragraph>
        Buscando Huellas se compromete a no ceder, vender, ni entregar a otras empresas o personas físicas, la información suministrada por los Usuarios.
Los Usuarios aceptan por el hecho de registrarse como tales en el Sitio, el derecho de Buscando Huellas de comunicarse con ellos en forma telefónica o vía electrónica; ello, hasta tanto los Usuarios hagan saber su decisión en contrario a Buscando Huellas por medio fehaciente. Esta autorización incluye la recepción de correos electrónicos, newsletter, alertas diarias, etc  .
 </Paragraph>
      <Heading tag="h4">9. RESPONSABILIDAD</Heading>
      <Paragraph>
        Los Usuarios resultan responsables de toda afirmación y/o expresión y/o acto celebrado con su nombre de usuario y contraseña.
Los Usuarios aceptan y reconocen que Buscando Huellas no será responsable, contractual ni extracontractualmente, por ningún daño o perjuicio, directo o indirecto, derivado de la utilización del Servicio y/o la publicación de Avisos, su contenido o de cualquier relación que surga como consecuencia de la interacción entre usuarios y/o usuarios y visitantes del sitio.
</Paragraph>
      <Heading tag="h4">10. INDEMNIDAD</Heading>
      <Paragraph>
        Los Usuarios asumen total responsabilidad frente a Buscando Huellas y a terceros por los daños y/o perjuicios de toda clase que se generen como consecuencia del uso del Servicio, debiendo indemnizar y mantener indemne a Buscando Huellas y a terceros ante cualquier reclamo (incluyendo honorarios profesionales costos y costas) que pudiera corresponder en los supuestos indicados.
</Paragraph>
      <Heading tag="h4">11. MODIFICACIÓN DEL SERVICIO O DE LOS TÉRMINOS Y CONDICIONES</Heading>
      <Paragraph>
        Buscando Huellas se reserva el derecho a modificar el Servicio, los Términos y condiciones y las normas que los complementan, en cualquier momento y cuantas veces lo crea conveniente, sin necesidad de notificar en forma previa a los Usuarios.
Queda entendido y así deberá ser interpretado en caso de conflicto que es obligación del Usuario revisar periódicamente las normas que reglamentan el Servicio.
Los Usuarios no podrán hacer responsable a Buscando Huellas y/o a ningún tercero por la suspensión o terminación del Servicio, a excepción del cumplimiento de los servicios o productos publicados y pagados.
</Paragraph>
      <Heading tag="h4">12. LOS USUARIOS EXPRESAMENTE COMPRENDEN Y ESTÁN DE ACUERDO EN QUE:</Heading>
      <Paragraph>
        a. La utilización del Servicio es a su solo riesgo;
b. ROCABELL S.R.L. no garantiza que el Servicio sea el adecuado a sus necesidades;
c. El Servicio puede ser suspendido o interrumpido;
d. El Servicio puede contener errores;
e. Buscando Huellas no será responsable por ningún daño o perjuicio, directo o indirecto, incluyendo, sin ningún tipo de limitación, daños producidos por la pérdida o deterioro de información;
f. Los Usuarios son los únicos responsables de los contenidos de la información que se publica a través del Servicio, frente a Buscando Huellas y a terceras personas sean estas físicas o jurídicas.
g. Buscando Huellas se reserva el derecho a suspender, limitar y/o terminar el Servicio.
h. El Servicio puede no siempre estar disponible debido a dificultades técnicas o fallas de Internet, o por cualquier otro motivo ajeno a Buscando Huellas, motivo por el cual no podrá imputársele a responsabilidad alguna.
i. El contenido de las distintas pantallas del Sitio junto con, y sin que se considere una limitación, sus programas, bases de datos, redes y archivos, son de propiedad de Buscando Huellas . Su uso indebido así como su reproducción no autorizada podrá dar lugar a las acciones judiciales que correspondan.
j. La utilización del Servicio no podrá, en ningún supuesto, ser interpretada como una autorización y/o concesión de licencia para la utilización de los derechos intelectuales de Buscando Huellas y/o de un tercero.
k. La utilización de Internet en general y del Sitio en particular, implica la asunción de riesgos de potenciales daños al software y al hardware del Usuario.
Por tal motivo, el equipo terminal desde el cual acceda al Sitio el Usuario, estaría en condiciones de resultar atacado y dañado por la acción de hackers quienes podrían incluso acceder a la información contenida en el equipo terminal del Usuario, extraerla, sustraerla y/o dañarla.
l. Paralelamente, el intercambio de información a través de Internet tiene el riesgo de que tal información pueda ser captada por un tercero. Buscando Huellas no se hace responsable de las consecuencias que pudiera acarrear al Usuario tal hipótesis.
m. Buscando Huellas no guarda obligación alguna de conservar información que haya hecho disponible a los Usuarios, ni que le haya sido enviada por éstos últimos.
</Paragraph>
      <Heading tag="h4">13. LEY APLICABLE Y TRIBUNAL COMPETENTE</Heading>
      <Paragraph>
        Los presentes Términos y condiciones y las normas que lo complementan constituyen un acuerdo legal entre los Usuarios y Buscando Huellas, al cual le serán aplicadas las leyes de la República Argentina, siendo competentes para cualquier controversia que pudiere llegar a suscitarse, los tribunales nacionales en lo comercial, con asiento en la Ciudad  de Córdoba.
La utilización del Servicio está expresamente prohibida en toda jurisdicción en donde no puedan ser aplicadas las condiciones establecidas en los presentes Términos y condiciones.
Si los Usuarios utilizan el Servicio significa que han leído, entendido y acordado las normas antes expuestas. Si no están de acuerdo con ellas, deben abstenerse de hacer uso del servicio.
Toda notificación u otra comunicación que deba efectuarse bajo estos Términos y condiciones, deberá realizarse por escrito: (i) al Usuario: a la cuenta de correo electrónico por él ingresada o (ii) a Buscando Huellas: a la cuenta de correo electrónico contacto@buscandohuellas.com o a ROCABELL S.R.L, BOLIVA 911, CIUDAD DE GENERAL ROCA, PCIA. DE RIO NEGRO
      </Paragraph>
    </Box>
  </HomeLayout>
)

export default Terms