import Router from 'next/router'

//Grommet Comps
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Paragraph from 'grommet/components/Paragraph'
import Button from 'grommet/components/Button'

const AddVolunteerText = (props) => (
  <Box align="center">
    <Heading tag="h3" strong={true} margin="small">¿Te gustaría ser voluntario?</Heading>
    <Paragraph align="center">
      Ser voluntario significa que te notificaremos cada vez que alguien publique dentro de
          tu zona una mascota perdida, encontrada, en adopción o en emergencia para que puedas
          brindarles ayuda. Puedes elegir en qué zona geográfica y en qué categorías ser voluntario.
          ¿Te gustaría ayudar a los animales de tu ciudad?
    </Paragraph>
    <Button primary={true} label="¡Quiero ser voluntario!" onClick={() => { Router.push('/setvolunteer') }} />
  </Box>
)

export default AddVolunteerText