import React, { Component } from 'react'
import nextCookies from '../lib/nextCookies'
import { User, Searches } from '../services/API'
import { toast } from 'react-toastify';

import Link from 'next/link'
import redirect from '../lib/redirect'
import Joyride from 'react-joyride'

import Router from 'next/router'


//Grommet Components
import Article from 'grommet/components/Article'
import Section from 'grommet/components/Section'
import Tiles from 'grommet/components/Tiles'
import Spinning from 'grommet/components/icons/Spinning'
import Header from 'grommet/components/Header'
import Add from 'grommet/components/icons/base/Add'
import Anchor from 'grommet/components/Anchor'
import ListPlaceholder from 'grommet-addons/components/ListPlaceholder'
import Button from 'grommet/components/Button'
import Paragraph from 'grommet/components/Paragraph'

//App Components
import Layout from '../components/Layout'
import UserSidebar from '../components/UserSidebar'
import ProfileHeader from '../components/ProfileHeader'
import SearchTile from '../components/SearchTile'
import FoundModal from '../components/FoundModal'
import DeleteModal from '../components/DeleteModal'
import ShareModal from '../components/ShareModal'
import SearchControls from '../components/SearchControls'


class UserSearches extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      searches: [],
      foundModal: null,
      foundModalCat: null,
      foundModalLoading: false,
      runJoyride: false
    }
  }

  static async getInitialProps(ctx) {
    const user = await User.getCurrentUser(ctx)
    if (!user) {
      redirect(ctx, '/login?redirect=/usersearches')
    } else if (!user.email_confirmed) {
      redirect(ctx, '/userpanel?message=Por favor confirma tu email para continuar utilizando la plataforma.')
    }
    return {
      user: user
    }
  }

  componentDidMount() {
    Searches.getUserSearches(this.props.user.id, (result) => {
      this.setState({
        searches: result,
        loading: false
      })
    })
  }

  async deleteSearch(search_id, index) {
    let res = await Searches.deleteSearch(search_id)
    if (res.ok) {
      toast.info("La búsqueda fue eliminada correctamente.", {
        position: toast.POSITION.BOTTOM_CENTER
      })
      let searchesArray = this.state.searches
      searchesArray[ index ].hidden = true
      this.setState({
        searches: searchesArray,
        deleteModal: null
      })
    }
  }

  async setFoundSearch(search_id) {
    let res = await Searches.setFoundSearch(search_id)
    if (res.ok) {
      Router.push('/congrats')
    }
  }

  async renewSearch(search, index) {
    let res = await Searches.renewSearch(search.id)
    if (res.ok) {
      let searchesArray = this.state.searches
      searchesArray[ index ].expired = false
      this.setState({
        searches: searchesArray
      })
      toast.success("La búsqueda fue renovada correctamente.", {
        position: toast.POSITION.BOTTOM_CENTER
      })
    }
  }

  runJoyride = () => {
    setTimeout(() => {
      this.setState({
        runJoyride: true
      })
    }, 600)
  }

  openFoundModal = (search_id, category) => {
    this.setState({ foundModal: search_id, foundModalCat: category })
  }

  closeFoundModal = () => {
    this.setState({ foundModal: null })
  }

  confirmFoundModal = () => {
    this.setState({
      foundModalLoading: true
    })
    this.setFoundSearch(this.state.foundModal)
  }

  openDeleteModal = (search_id, index) => {
    this.setState({ deleteModal: search_id, deleteModalIndex: index })
  }

  closeDeleteModal = () => {
    this.setState({ deleteModal: null })
  }

  confirmDeleteModal = () => {
    this.deleteSearch(this.state.deleteModal, this.state.deleteModalIndex)
  }

  openShareModal = (search) => {
    this.setState({ shareModal: search })
  }

  closeShareModal = () => {
    this.setState({ shareModal: null })
  }

  joyrideSteps = [
    {
      title: 'Tutorial',
      text: `
        Desde el menu de Opciones, puedes compartir tu búsqueda en las redes sociales,
        marcar que se dio el reencuentro o eliminarla.
      `,
      selector: '.searchOptions0'
    },
    {
      title: 'Tutorial',
      text: `
        Haz click en Ver Resultados para ir a ver los resultados que coinciden con tu búsqueda.
      `,
      selector: '.seeResults0'
    }
  ];

  render() {
    return (
      <div>
        <Joyride
          ref={c => (this.joyride = c)}
          resizeDebounce={true}
          locale={{
            back: (<span>Atras</span>),
            close: (<span>Cerrar</span>),
            last: (<span>Listo</span>),
            next: (<span>Siguiente</span>),
            skip: (<span>Omitir</span>),
          }}
          run={this.state.runJoyride && !User.getTutorialDone()}
          autoStart={true}
          steps={this.joyrideSteps}
          type='continuous'
          scrollToSteps={false}
        />
        <Layout user={this.props.user} pathname={this.props.url.pathname} title="Mis Búsquedas">
          <Article full={true} colorIndex="light-2">
            <ProfileHeader title="Mis Búsquedas" />
            {
              this.state.foundModal &&
              <FoundModal
                loading={this.state.foundModalLoading}
                onConfirm={this.confirmFoundModal}
                onDismiss={this.closeFoundModal}
                category={this.state.foundModalCat} />
            }
            {
              this.state.deleteModal &&
              <DeleteModal onConfirm={this.confirmDeleteModal} onDismiss={this.closeDeleteModal} />
            }
            {
              this.state.shareModal &&
              <ShareModal onDismiss={this.closeShareModal} search={this.state.shareModal} />
            }
            {
              this.state.loading ?
                <Section flex="grow" direction="column" justify="center" align="center">
                  <Spinning size="large" />
                </Section>
                :
                <Section alignContent="center">
                  <Tiles flush={false} justify="center" fill={true}>
                    <ListPlaceholder
                      emptyMessage="No tienes ninguna búsqueda en proceso."
                      unfilteredTotal={this.state.searches.length}
                      filteredTotal={this.state.searches.length}
                      addControl={
                        <Link prefetch href="/addsearch">
                          <Button icon={<Add />}
                            label="Nueva Búsqueda"
                            primary={true}
                          />
                        </Link>
                      }
                    />
                    {this.state.searches.map((search, index) => {
                      return (
                        <SearchTile key={search.id} search={search} index={index} runJoyride={this.runJoyride}>
                          <Header size='small'>
                            {
                              search.found ?
                                <div className="foundLabel">¡Reencontrado!</div>
                                :
                                <SearchControls
                                  search={search} index={index}
                                  openDeleteModal={this.openDeleteModal}
                                  openFoundModal={this.openFoundModal}
                                  openShareModal={this.openShareModal}
                                  renewSearch={this.renewSearch.bind(this)}
                                />
                            }
                          </Header>
                        </SearchTile>
                      )
                    })}
                  </Tiles>
                  <Paragraph size="small" align="center" style={{ margin: 'auto' }}>
                    Es necesario renovar las búsquedas cada 30 días, cuando una búsqueda este por expirar, te notificaremos para que ingreses a renovarla.
                  </Paragraph>
                </Section>
            }
          </Article>
        </Layout>
      </div>
    )
  }
}

export default UserSearches