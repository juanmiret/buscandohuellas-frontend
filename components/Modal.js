import React from 'react'

//Grommet Components
import Button from 'grommet/components/Button'
import Close from 'grommet/components/icons/base/Close'

export default class Modal extends React.Component {
  dismiss(e) {
    if (this._shim === e.target ||
      this._photoWrap === e.target) {
      if (this.props.onDismiss) {
        this.props.onDismiss()
      }
    }
  }

  render() {
    return (
      <div ref={el => (this._shim = el)} className='shim' onClick={(e) => this.dismiss(e)}>
        <div ref={el => (this._photoWrap = el)} className='content'>
          <Button className="closeModal" icon={<Close />} onClick={this.props.onDismiss} />
          {this.props.children}
        </div>
        <style jsx>{`
          .shim {
            display: flex;
            justify-content: center;
            align-items: center;
            position: fixed;
            background: rgba(0,0,0,.65);
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
            z-index: 501;
          }
          .content {
            position: relative;
            background: white;
            border-radius: 10px;
          }
        `}</style>
      </div>
    )
  }
}