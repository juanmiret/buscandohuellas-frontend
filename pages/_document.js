// ./pages/_document.js
import Document, { Head, Main, NextScript } from 'next/document'
import flush from 'styled-jsx/server'
import stylesheet from '../styles/index.scss'

export default class MyDocument extends Document {
  static getInitialProps({ renderPage }) {
    const { html, head, errorHtml, chunks } = renderPage()
    const styles = flush()
    return { html, head, errorHtml, chunks, styles }
  }

  render() {
    const script = `
      window.ENV = '${process.env.ENV || 'dev'}';
      UPLOADCARE_LOCALE = 'es';
      UPLOADCARE_PUBLIC_KEY = '${UPLOADCARE_PUBLIC_KEY}';
    `;
    return (
      <html>
        <Head>
          <meta name="description" content="La plataforma más efectiva del mundo para mascotas perdidas, en adopción o en estado de emergencia." />
          <script dangerouslySetInnerHTML={{ __html: script }} />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link rel="apple-touch-icon" sizes="76x76" href="/static/favicons/apple-touch-icon.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="/static/favicons/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="/static/favicons/favicon-16x16.png" />
          <link rel="manifest" href="/static/favicons/manifest.json" />
          <link rel="mask-icon" href="/static/favicons/safari-pinned-tab.svg" color="#5bbad5" />
          <meta name="theme-color" content="#ffffff" />
          <link href="https://ucarecdn.com/dbec53f5-fd20-474b-89df-5f491379d8b7/grommetmin.css" rel="stylesheet" type="text/css" />
          <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
          <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1KziCXqIjYninVGbqE2g37siJ2UZNQaw&libraries=places"></script>
          <script src="https://ucarecdn.com/libs/widget/2.10.3/uploadcare.full.min.js" charset="utf-8"></script>
        </Head>
        <body className="custom_class">
          {this.props.customValue}
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}