import SimpleLocation from '../components/SimpleLocation'
import Link from 'next/link'

//Gromet Comps
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import CheckBox from 'grommet/components/CheckBox'
import FormField from 'grommet/components/FormField'
import Anchor from 'grommet/components/Anchor'
import Paragraph from 'grommet/components/Paragraph'
import Button from 'grommet/components/Button'

const VolunteerData = (props) => (
  <Box size="large" align="stretch">
    <Heading tag="h4" strong align="center">Eres voluntario en la siguiente zona: </Heading>
    <SimpleLocation center={{ lat: props.lat, lng: props.lng }} radius={props.radius} withRadius height="300px" />
    <Heading tag="h4" strong align="center" margin="small">Y en las siguientes categorías: </Heading>
    <FormField>
      <Box direction="row" pad="small">
        <CheckBox label="Perdidos" checked={props.categories.includes('lost')} disabled />
        <CheckBox label="Encontrados" checked={props.categories.includes('found')} disabled />
        <CheckBox label="En Adopción" checked={props.categories.includes('adoption')} disabled />
        <CheckBox label="Emergencias" checked={props.categories.includes('emergency')} disabled />
      </Box>
    </FormField>
    <Box align="center" pad="small">
      <Link href="/myzone">
        <Button primary label="Ver publicaciones en Mi Zona" />
      </Link>
      <div style={{ height: '15px' }}></div>
      <Link href="/setvolunteer">
        <Anchor label="Cambiar configuración" />
      </Link>
      <Anchor label="Dejar de ser voluntario" onClick={() => {
        props.unsetVolunteer()
      }} />
    </Box>
  </Box>
)

export default VolunteerData