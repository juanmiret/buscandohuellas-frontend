import HomeLayout from '../components/HomeLayout'
import Link from 'next/link'

//Grommet Components
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Paragraph from 'grommet/components/Paragraph'
import Button from 'grommet/components/Button'
import Image from 'grommet/components/Image'

const Congrats = (props) => (
  <HomeLayout>
    <Box full={true} justify="center" align="center" pad="small">
      <Image src="/static/buscandohuellas_logo_azul.png" size="medium" />
      <Heading tag="h1" strong={true}>¡Bienvenido a la familia!</Heading>
      <Paragraph align="center">
        Muchas gracias por decidir formar parte de Buscando Huellas, en breve nos pondremos
        en contacto contigo para ultimar detalles de tu perfil y conocernos mejor.
      </Paragraph>
      <Link href="/userpanel">
        <Button primary={true} label="Volver a la plataforma" />
      </Link>
    </Box>
  </HomeLayout>
)

export default Congrats