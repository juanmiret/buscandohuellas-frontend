import React from 'react'
import { User } from '../services/API'
import redirect from '../lib/redirect'
import Router from 'next/router'

// Grommet Comps
import Box from 'grommet/components/Box'
import Button from 'grommet/components/Button'
import LinkPrevious from 'grommet/components/icons/base/LinkPrevious'

// App Comps
import Layout from '../components/Layout'
import LocationStep from '../components/addpub/LocationStep'
import CategoriesStep from '../components/setvolunteer/CategoriesStep'
import DescriptionStep from '../components/setvolunteer/DescriptionStep'

import Joyride from 'react-joyride'

export default class AddVolunteer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      stepIndex: 0,
      isVolunteer: false,
      runJoyride: false,
      volunteerData: {
        address: null,
        lat: null,
        lng: null,
        radius: null,
        categories: []
      }
    }
  }

  static async getInitialProps(ctx) {
    const user = await User.getCurrentUser(ctx)
    if (!user) {
      redirect(ctx, '/login')
    } else {
      return {
        user: user
      }
    }
  }

  componentWillMount() {
    if (this.props.user.isVolunteer) {
      this.setState({
        isVolunteer: true,
        volunteerData: this.props.user.volunteerData
      })
    }
  }


  handleNext = (state) => {
    const { stepIndex } = this.state;
    this.setState({
      stepIndex: stepIndex + 1,
      volunteerData: {
        ...this.state.volunteerData,
        ...state
      }
    });
  };

  handlePrev = () => {
    const { stepIndex } = this.state;
    this.setState({
      stepIndex: stepIndex - 1
    })
  };

  handleSubmit = ({ categories }) => {
    let volunteerData = {
      ...this.state.volunteerData,
      categories: categories
    }
    this.submit(volunteerData);
  }

  async submit(volunteerData) {
    let res = await User.setVolunteer(volunteerData)
    if (res.ok) {
      Router.push('/userpanel?volunteer_message=true')
    }
  }

  getStepContent = () => {
    switch (this.state.stepIndex) {
      case 0:
        return (
          <LocationStep
            volunteerData={this.state.isVolunteer && this.state.volunteerData}
            nextHandler={this.handleNext} prevHandler={this.handlePrev}
            withRadius
            runJoyride={this.runJoyride} />
        )
        break
      case 1:
        return (
          <CategoriesStep
            categories={this.state.volunteerData.categories}
            nextHandler={this.handleSubmit}
            prevHandler={this.handlePrev} />
        )
        break
      default:
        return ''
    }
  }

  runJoyride = () => {
    setTimeout(() => {
      this.setState({
        runJoyride: true
      })
    }, 600)
  }

  joyrideSteps = [
    {
      title: 'Ingresa la zona en la que serás voluntario.',
      text: `
        Ingresa una ubicación y selecciona una opción de la lista.
        Asegúrate de escribir la dirección sin esquinas. También puedes 
        escribir una ciudad y luego arrastrar el marcador hasta la
        ubicación deseada. Asegúrate de que la ubicación sea correcta antes de
        continuar.
      `,
      selector: '.geosuggest'
    }
  ];

  render() {
    return (
      <div>
        <Joyride
          ref={c => (this.joyride = c)}
          resizeDebounce={true}
          locale={{
            back: (<span>Atras</span>),
            close: (<span>Cerrar</span>),
            last: (<span>Listo</span>),
            next: (<span>Siguiente</span>),
            skip: (<span>Omitir</span>),
          }}
          run={this.state.runJoyride}
          autoStart={true}
          steps={this.joyrideSteps}
          type='continuous'
          allowClicksThruHole={true}
        />
        <Layout user={this.props.user} title="Ser Voluntario">
          <Box pad="small" align="center" justify="center" full={true} className="addPubWrapper">
            {this.getStepContent()}
            <Button icon={<LinkPrevious />} className={this.state.stepIndex > 0 && this.state.stepIndex < 5 ? 'prevStepButton' : 'hidden'} onClick={this.handlePrev} />
          </Box>
        </Layout>
      </div>
    )
  }
}