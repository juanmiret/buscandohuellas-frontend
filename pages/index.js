import React, { Component } from 'react'
import Router from 'next/router'
import Link from 'next/link'

//Grommet Comps
import Button from 'grommet/components/Button'
import Anchor from 'grommet/components/Anchor'
import LoginIcon from 'grommet/components/icons/base/Login'
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Paragraph from 'grommet/components/Paragraph'
import Image from 'grommet/components/Image'
import Down from 'grommet/components/icons/base/Down'
import Header from 'grommet/components/Header'
import Footer from 'grommet/components/Footer'
import Menu from 'grommet/components/Menu'
import SocialFacebook from 'grommet/components/icons/base/SocialFacebook'
import SocialTwitter from 'grommet/components/icons/base/SocialTwitter'
import SocialInstagram from 'grommet/components/icons/base/SocialInstagram'



//App Comps
import HomeLayout from '../components/HomeLayout'
import LoginButtons from '../components/LoginButtons'
import Modal from '../components/Modal'

import ResponsiveEmbed from 'react-responsive-embed'



class Index extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <HomeLayout>
        <Box align="center" justify="center" colorIndex="neutral-1-a">
          <Image src='/static/buscandohuellas_logo.png' />
          <Heading tag="h4" margin="small" align="center" strong>
            La plataforma más efectiva del mundo para encontrar mascotas perdidas, en adopción y en estado de emergencia
          </Heading>
          <Paragraph align="center">
            ¿Perdiste, encontraste, quieres dar en adopción o adoptar una mascota?<br />
            Estás en el lugar indicado.
          </Paragraph>
          <Box direction="row" justify="between">
            <Box margin="medium" justify="center" align="center" colorIndex="neutral-1" pad="medium" className="statisticsBox">
              <Heading strong tag="h3" margin="none">+120.000</Heading>
              <Heading tag="h4">Usuarios</Heading>
            </Box>
            <Box margin="medium" justify="center" align="center" colorIndex="neutral-1" pad="medium" className="statisticsBox">
              <Heading strong tag="h3" margin="none">+15.000</Heading>
              <Heading tag="h4" justify="center" align="center">Búsquedas <br />Activas</Heading>
            </Box>
            <Box margin="medium" justify="center" align="center" colorIndex="neutral-1" pad="medium" className="statisticsBox">
              <Heading strong tag="h3" margin="none">+11.000</Heading>
              <Heading tag="h4">Voluntarios</Heading>
            </Box>
            <Box margin="medium" justify="center" align="center" colorIndex="neutral-1" pad="medium" className="statisticsBox">
              <Heading strong tag="h3" margin="none">+2500</Heading>
              <Heading tag="h4" align="center">Reencuentros y <br />Adopciones</Heading>
            </Box>
          </Box>
          <Down size="large" className="homeDownIcon" />
        </Box>
        <Box align="center" justify="center" colorIndex="neutral-1-a">
          <Heading tag="h2" margin="large" align="center" strong>¿Como funciona Buscando Huellas?</Heading>
          <Box size="large">
            <div className="video-container">
              <iframe src="https://www.youtube.com/embed/SAdQ7ZbmffE?rel=0" width="640" height="360" frameBorder="0" allowFullScreen></iframe>
            </div>
          </Box>
        </Box>
        <Box pad="medium" align="center" justify="center" colorIndex="neutral-1-a">
          <Heading tag="h4" align="center" strong margin="medium">Para comenzar, haz click en el siguiente botón:</Heading>
          <Link prefetch href='/login'>
            <Button primary icon={<LoginIcon />} label="Iniciar sesión" className="heroLoginButton contactButton" />
          </Link>
          <Heading tag="h4" align="center" strong margin="medium">Nuestras redes sociales:</Heading>
          <Box direction="row" align="center" justify="center" responsive={false}>
            <Anchor icon={<SocialFacebook />} href="https://facebook.com/buscandohuellasok" target="_blank" />
            <Anchor icon={<SocialTwitter />} href="https://twitter.com/buscandohue_ok" target="_blank" />
            <Anchor icon={<SocialInstagram />} href="https://instagram.com/buscandohuellasok" target="_blank" />
          </Box>
        </Box>

        <Footer justify="center" size="large" colorIndex="neutral-1-a">
          <Box direction='row'
            align='center'
            justify="center"
            pad={{ "between": "medium", vertical: "large" }}>
            <Paragraph margin='none'>
              © 2017 Buscando Huellas
          </Paragraph>
            <Anchor href="https://buscandohuellas.com/usersearches">
              Perdí / Encontré
              </Anchor>
            <Anchor href="https://buscandohuellas.com/adoptions">
              Adopciones
            </Anchor>
            <Anchor href="https://buscandohuellas.com/emergencies">
              Emergencias
            </Anchor>
            <Anchor href="https://buscandohuellas.com/login">
              Voluntarios
            </Anchor>
            <Anchor href="mailto:contacto@buscandohuellas.com">
              Contactanos
            </Anchor>
          </Box>
        </Footer>

      </HomeLayout >
    )
  }
}

export default Index