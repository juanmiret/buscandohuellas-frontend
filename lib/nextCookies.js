/**
 * Module dependencies
 */

const Cookies = require('js-cookie')
const parser = require('cookie')

/**
 * Export `nextCookies`
 */

module.exports = nextCookies

/**
 * nextCookies function
 */

function nextCookies(ctx, options) {
  options = options || {}
  if (ctx.req) {
    // server
    const cookies = ctx.req.headers.cookie
    if (!cookies) return {}
    return parser.parse(cookies, options)
  } else {
    // browser
    return Cookies.get()
  }
}