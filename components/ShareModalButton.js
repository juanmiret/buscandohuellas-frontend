import React from 'react'
import ShareModal from '../components/ShareModal'

//Grommet Comps
import Box from 'grommet/components/Box'
import Image from 'grommet/components/Image'
import Heading from 'grommet/components/Heading'
import Anchor from 'grommet/components/Anchor'
import Share from 'grommet/components/icons/base/Share'

export default class ShareModalButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false
    }
  }

  dismissModal = () => {
    this.setState({
      open: false
    })
  }

  openModal = () => {
    this.setState({
      open: true
    })
  }

  render() {
    return (
      <Box align="center" pad="small">
        {
          this.state.open &&
          <ShareModal onDismiss={this.dismissModal} search={this.props.search} />
        }
        <Anchor icon={<Share />} label="Compartir" onClick={this.openModal} />
      </Box>
    )
  }
}