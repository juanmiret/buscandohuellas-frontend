import React from 'react'

//Grommet Components
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import CheckBox from 'grommet/components/CheckBox'
import Button from 'grommet/components/Button'
import Spinning from 'grommet/components/icons/Spinning'
import LinkNext from 'grommet/components/icons/base/LinkNext'
import Form from 'grommet/components/Form'
import FormField from 'grommet/components/FormField'

export default class CategoriesStep extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      categories: []
    }
  }

  componentDidMount() {
    if (this.props.categories) {
      this.setState({
        categories: this.props.categories
      })
    }
  }

  submit = () => {
    this.setState({
      loading: true
    })
    this.props.nextHandler({ categories: this.state.categories })
  }

  handleChange = (event) => {
    let categories = this.state.categories
    if (categories.includes(event.target.name)) {
      categories.splice(categories.indexOf(event.target.name), 1)
      this.setState({
        categories: categories
      })
    } else {
      categories.push(event.target.name)
      this.setState({
        categories: categories
      })
    }
  }

  render() {
    return (
      <Box align="center">
        <Heading tag="h3" align="center">Selecciona las categorías en las que quieres ser voluntario: </Heading>
        <Form compact={true} pad="small">
          <FormField>
            <CheckBox label="Perdidos" name="lost" onChange={this.handleChange} checked={this.state.categories.includes('lost')} toggle={true} />
            <CheckBox label="Encontrados" name="found" onChange={this.handleChange} checked={this.state.categories.includes('found')} toggle={true} />
            <CheckBox label="En Adopción" name="adoption" onChange={this.handleChange} checked={this.state.categories.includes('adoption')} toggle={true} />
            <CheckBox label="Emergencias" name="emergency" onChange={this.handleChange} checked={this.state.categories.includes('emergency')} toggle={true} />
          </FormField>
        </Form>
        <Button
          icon={
            this.state.loading ? <Spinning className="whiteSpinning" /> : <LinkNext />
          }
          label="CONTINUAR"
          primary={true}
          onClick={this.submit} />
      </Box>
    )
  }
}