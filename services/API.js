import Router from 'next/router'
import Cookies from 'js-cookie'
import nextCookies from '../lib/nextCookies'
import fetch from 'isomorphic-unfetch'

const URL = BACKEND_URL

export class Vets {

  static async newVet(formData) {
    let request_url = `${URL}/vet/add`
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        vet_data: formData
      })
    }
    const res = await fetch(request_url, init)
    const json = await res.json()

    return json
  }

  static async getVets(location) {
    let request_url = `${URL}/vets`;
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        location: location
      })
    }
    const res = await fetch(request_url, init)
    const json = await res.json()

    return json
  }
}

export class User {

  static async resendEmailConfirmation(email) {
    let request_url = `${URL}/resend_confirmation`
    let headers = {
      'Content-Type': 'application/json'
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        email: email
      })
    }
    const res = await fetch(request_url, init)
    const json = await res.json()
    return json
  }

  static async updateEmail(email) {
    let request_url = `${URL}/update_account_email`
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        email: email
      })
    }
    const res = await fetch(request_url, init)
    const json = await res.json()
    return json
  }

  static async signUpWithEmail(fields) {
    let request_url = `${URL}/create_account_email`
    let headers = {
      'Content-Type': 'application/json'
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        name: fields.name,
        email: fields.email,
        password: fields.password
      })
    }
    const res = await fetch(request_url, init)
    const json = await res.json()
    return json
  }

  static async loginWithEmail(email, password, redirectPath) {
    let request_url = `${URL}/login_with_email`
    let headers = {
      'Content-Type': 'application/json'
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        email: email,
        password: password
      })
    }
    const res = await fetch(request_url, init)
    const json = await res.json()
    if (json.token) {
      Cookies.set('token', json.token, {
        expires: 365
      })
      if (redirectPath) {
        Router.push(redirectPath)
      } else {
        Router.push('/userpanel')
      }
    } else {
      return json
    }
  }

  static async setVolunteer(volunteer_data) {
    let request_url = `${URL}/user/set_volunteer`
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(volunteer_data)
    }
    const res = await fetch(request_url, init)
    return res
  }

  static async unsetVolunteer() {
    let request_url = `${URL}/user/unset_volunteer`
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'POST',
      headers: headers,
    }
    const res = await fetch(request_url, init)
    return res
  }

  static getTutorialDone() {
    let basic_tutorial_done = Cookies.get('basic_tutorial_done')
    return basic_tutorial_done
  }

  static setTutorialDone() {
    Cookies.set('basic_tutorial_done', true, { expires: 365 })
  }

  static responseFacebook(response, redirectPath) {
    let request_url = `${URL}/loginFacebook`;
    let headers = {
      'Content-Type': 'application/json'
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        profile: response
      })
    }
    fetch(request_url, init)
      .then((res) => {
        return res.json()
      })
      .then((result) => {
        if (result.token) {
          Cookies.set('token', result.token, {
            expires: 365
          })
          if (redirectPath) {
            Router.push(redirectPath)
          } else {
            Router.push('/userpanel')
          }
        }
      })
      .catch(function (error) {
        console.log('Request failed', error);
      });
  }

  static responseGoogle(response, redirectPath) {
    let profile = response.getAuthResponse()
    let request_url = `${URL}/loginGoogle`
    let headers = {
      'Content-Type': 'application/json'
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        google_token: profile.access_token
      })
    }
    fetch(request_url, init)
      .then((res) => {
        return res.json()
      })
      .then((result) => {
        if (result.token) {
          Cookies.set('token', result.token, {
            expires: 365
          })
          if (redirectPath) {
            Router.push(redirectPath)
          } else {
            Router.push('/userpanel')
          }
        }
      })
  }

  static async getCurrentUser(ctx) {
    let { token } = nextCookies(ctx)
    if (token) {
      let request_url = `${URL}/user/get_current`
      let init = {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token
        }
      }
      const res = await fetch(request_url, init)
      const json = await res.json()
      return json.user
    } else {
      return false
    }
  }

  static logout() {
    Cookies.remove('token')
    Router.push('/')
  }
}

export class Searches {
  static async getAdoptions(location, query) {
    let request_url = `${URL}/searches`;
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        location: location,
        query: {
          ...query,
          category: 'adoption'
        }
      })
    }
    const res = await fetch(request_url, init)
    const json = await res.json()

    return json
  }

  static async getSearchDetails(search_id) {
    let request_url = `${URL}/search/${search_id}`;
    let init = {
      method: 'GET'
    }
    const res = await fetch(request_url, init)
    const json = await res.json()

    return json
  }

  static getUserSearches(user_id, cb) {
    let request_url = `${URL}/user/my_searches`;
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'GET',
      headers: headers
    }
    fetch(request_url, init)
      .then((res) => {
        return res.json()
      })
      .then((result) => {
        return cb(result)
      })
      .catch(function (error) {
        console.log('Request failed', error);
      });
  }

  static getSearchMatches(search_id, cb) {
    let request_url = `${URL}/search/${search_id}/matches`;
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'GET',
      headers: headers
    }
    fetch(request_url, init)
      .then((res) => {
        return res.json()
      })
      .then((result) => {
        return cb(result)
      })
      .catch(function (error) {
        console.log('Request failed', error);
      });
  }

  static createSearch(search_data, cb) {
    let request_url = `${URL}/search/add`
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        search_data: search_data
      })
    }
    fetch(request_url, init)
      .then((res) => {
        return res.json()
      })
      .then((result) => {
        return cb(result)
      })
      .catch(function (error) {
        console.log('Request failed', error);
      });
  }

  static async discardSearch(search_id, discard_id, user) {
    let request_url = `${URL}/search/${search_id}/discard`
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        discard_id: discard_id
      })
    }
    const res = await fetch(request_url, init)
    return res
  }

  static async deleteSearch(search_id) {
    let request_url = `${URL}/search/${search_id}/delete`
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'POST',
      headers: headers
    }
    const res = await fetch(request_url, init)
    return res
  }

  static async setFoundSearch(search_id) {
    let request_url = `${URL}/search/${search_id}/found`
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'POST',
      headers: headers
    }
    const res = await fetch(request_url, init)
    return res
  }

  static async renewSearch(search_id) {
    let request_url = `${URL}/search/${search_id}/renew`
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'POST',
      headers: headers
    }
    const res = await fetch(request_url, init)
    return res
  }

  static async getVolunteerSearches() {
    let request_url = `${URL}/user/volunteerSearches`;
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': Cookies.get('token')
    }
    let init = {
      method: 'GET',
      headers: headers,
    }
    const res = await fetch(request_url, init)
    const json = await res.json()

    return json
  }

}