import Button from 'grommet/components/Button'
import Contact from 'grommet/components/icons/base/Contact'

const ContactButton = (props) => (
  <Button
    className="contactButton"
    label="Contactar"
    primary={true}
    icon={<Contact />}
    onClick={props.onClick} />
)

export default ContactButton