import React, { Component } from 'react'

//Grommet Components
import Heading from 'grommet/components/Heading'
import Paragraph from 'grommet/components/Paragraph'
import Tile from 'grommet/components/Tile'
import Tiles from 'grommet/components/Tiles'
import Button from 'grommet/components/Button'
import Spinning from 'grommet/components/icons/Spinning';
import LinkNext from 'grommet/components/icons/base/LinkNext'
import Box from 'grommet/components/Box'
import Notification from 'grommet/components/Notification';

export default class UploadStep extends Component {

  constructor(props) {
    super(props);
    this.state = {
      gallery: null,
      showError: false
    }
  }

  componentDidMount() {
    var that = this
    var multiWidget = uploadcare.MultipleWidget('[role=uploadcare-uploader][data-multiple]');
    multiWidget.onUploadComplete((gallery) => {
      // Handle uploaded file info.
      that.setState({
        gallery: {
          cdn_url: gallery.cdnUrl,
          uuid: gallery.uuid,
        },
        showError: false
      })
    });
  }

  handleNextStep = () => {
    if (this.state.gallery && this.state.gallery.cdn_url) {
      this.props.nextHandler({ gallery: this.state.gallery })
    } else {
      this.setState({ showError: true })
    }
  }

  handlePrevStep = () => {
    this.props.prevHandler()
  }


  render() {
    return (
      <Box align="center" justify="center">
        {
          this.state.showError &&
          <Notification message="Sube al menos una imagen para continuar" status="critical" size="small" />
        }
        <Heading tag="h4" margin="small" align="center" strong={true}>Sube fotos de la mascota:</Heading>
        <Paragraph align="center" style={{ marginTop: '0' }}>Tip: <br />Puedes recortar las imagenes antes de subirlas <br />haciendo click sobre las mismas.</Paragraph>
        <input type="hidden" role="uploadcare-uploader"
          data-images-only="true"
          data-crop="true"
          data-multiple="true"
          data-multiple-max={4}
          data-image-shrink="1400x1400"
          data-tabs="file url facebook gdrive gphotos dropbox instagram"
          data-locale="es"
        />
        <Button icon={<LinkNext />} label="SIGUIENTE" primary={true} className="nextStepButton" onClick={this.handleNextStep} />
      </Box>
    )
  }
}