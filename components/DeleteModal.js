import Modal from '../components/Modal'

//Grommet Comps
import Box from 'grommet/components/Box'
import Image from 'grommet/components/Image'
import Heading from 'grommet/components/Heading'
import Button from 'grommet/components/Button'


const DeleteModal = (props) => (
  <Modal onDismiss={props.onDismiss}>
    <Box pad="large" align="center">
      <Heading tag="h3">¿Seguro?</Heading>
      <Button label="Si, eliminar" className="discardButton" primary={true} onClick={props.onConfirm} />
    </Box>
  </Modal>
)

export default DeleteModal