import Link from 'next/link'

// Grommet Comps
import Button from 'grommet/components/Button'
import Menu from 'grommet/components/Menu'
import Anchor from 'grommet/components/Anchor'
import More from 'grommet/components/icons/base/More'
import Update from 'grommet/components/icons/base/Update'
import Share from 'grommet/components/icons/base/Share'
import Checkmark from 'grommet/components/icons/base/Checkmark'
import Trash from 'grommet/components/icons/base/Trash'
import LinkNext from 'grommet/components/icons/base/LinkNext'

import DropdownMenu from 'react-dd-menu';


import ShareModal from '../components/ShareModal'

export default class SearchControls extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isMenuOpen: false
    }
  }

  toggle = () => {
    this.setState({ isMenuOpen: !this.state.isMenuOpen });
  };

  close = () => {
    this.setState({ isMenuOpen: false });
  };

  renderButtons = () => {
    let { search, renewSearch, index } = this.props
    if (search.expired) {
      return (
        <Anchor icon={<Update />} label="Renovar" primary={true} onClick={() => {
          renewSearch(search, index)
        }} />
      )
    } else {
      if ([ 'adoption', 'emergency' ].includes(search.category)) {
        return (
          <Link prefetch href={`/search?search_id=${search.id}`}>
            <Anchor className={`seeResults${index}`} primary={true}>Ver Publicación</Anchor>
          </Link>
        )
      } else {
        return (
          <Link prefetch href={`/searchmatches?search_id=${search.id}`}>
            <Anchor className={`seeResults${index}`} primary={true}>Ver Resultados</Anchor>
          </Link>
        )
      }
    }
  }

  getSuccessText = () => {
    switch (this.props.search.category) {
      case 'lost':
      case 'found':
        return '¡Se dio el reencuentro!'
        break
      case 'adoption':
        return '¡Se adoptó!'
        break
      case 'emergency':
        return 'Emergencia resuelta'
        break
    }
  }

  render() {
    let { search, index, openFoundModal, openDeleteModal, openShareModal } = this.props
    return (
      <div className="searchControls">
        <DropdownMenu
          isOpen={this.state.isMenuOpen}
          close={this.close}
          toggle={<Button primary className="contactButton" label="Menú" onClick={this.toggle} />}
          align="left"
          className={`searchOptions searchOptions${index}`}>
          <li>
            <Anchor icon={<Share />} label="Compartir" onClick={() => {
              openShareModal(search)
            }} />
          </li>
          <li>
            <Link prefetch href={`/search?search_id=${search.id}`}>
              <Anchor icon={<LinkNext />} label="Ver publicación" />
            </Link>
          </li>
          <li>
            <Anchor icon={<Checkmark />} label={this.getSuccessText()} onClick={() => {
              openFoundModal(search.id, search.category)
            }} />
          </li>
          <li>
            <Anchor icon={<Trash />} label="Eliminar" onClick={() => {
              openDeleteModal(search.id, index)
            }} />
          </li>
        </DropdownMenu>
        {this.renderButtons()}
      </div>
    )
  }
}
