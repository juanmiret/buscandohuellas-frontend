import React from 'react'
import Router from 'next/router'
import { User, Adoptions } from '../services/API'

//App Comps
import Layout from '../components/Layout'
import ProfileHeader from '../components/ProfileHeader'

//Grommet Comps
import Button from 'grommet/components/Button'
import Article from 'grommet/components/Article'
import Section from 'grommet/components/Section'
import Quote from 'grommet/components/Quote';
import Paragraph from 'grommet/components/Paragraph'
import Heading from 'grommet/components/Heading'
import Alert from 'grommet/components/icons/base/Alert'
import Box from 'grommet/components/Box'


export default class AdoptionsPage extends React.Component {
  constructor(props) {
    super(props)
  }

  static async getInitialProps(ctx) {
    const user = await User.getCurrentUser(ctx)
    return {
      user: user
    }
  }

  render() {
    return (
      <Layout user={this.props.user} pathname={this.props.url.pathname} title="Emergencias">
        <Article>
          <ProfileHeader title="Emergencias" />
          <Section align="center" justify="center">
            <Alert size="large" />
            <Paragraph align="center">
              Publica en esta sección sólo animales en estado de emergencia que necesitan ayuda urgente.
              Notificaremos a todos los voluntarios de la zona para que puedan tomar acción.
            </Paragraph>
            <Heading tag="h4">NO publiques mascotas perdidas en esta sección.</Heading>
            <Button label="Publicar emergencia" primary={true} onClick={() => Router.push('/addsearch?category=emergency')} className="discardButton" />
            {
              this.props.user.isVolunteer ?
                <Box align="center">
                  <Paragraph align="center">
                    Ya eres voluntario, ingresa al panel Mi Zona para ver las Emergencias de tu zona.
                  </Paragraph>
                  <Button label="Mi Zona" primary={true} onClick={() => Router.push('/myzone')} />
                </Box>
                :
                <Box align="center">
                  <Paragraph align="center">
                    Si quieres ayudar, ver y participar en las Emergencias de tu zona, puedes ser voluntario
                    haciendo click en el siguiente botón.
                  </Paragraph>
                  <Button label="Quiero ser voluntario" primary={true} onClick={() => Router.push('/setvolunteer')} />
                </Box>
            }
          </Section>
        </Article>
      </Layout >
    )
  }

}