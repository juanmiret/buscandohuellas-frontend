import Header from 'grommet/components/Header'
import Title from 'grommet/components/Title'
import Box from 'grommet/components/Box'
import Menu from 'grommet/components/Menu'
import Actions from 'grommet/components/icons/base/Actions'
import Anchor from 'grommet/components/Anchor'

const PublicSearchHeader = (props) => (
  <Header pad="medium" colorIndex="neutral-1-a" fixed={true}>
    <Title>
      {props.title}
    </Title>
    <Box flex={true}
      justify='end'
      direction='row'
      responsive={false}>
    </Box>
  </Header>
)

export default PublicSearchHeader