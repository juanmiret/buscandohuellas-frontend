import Modal from '../components/Modal'
// Other Comps
import { ShareButtons, generateShareIcon } from 'react-share'
//Grommet Comps
import Box from 'grommet/components/Box'
import Image from 'grommet/components/Image'
import Heading from 'grommet/components/Heading'
import Button from 'grommet/components/Button'

//Helpers
import renderThumbnail from '../helpers/renderThumbnail'

const {
  FacebookShareButton,
  GooglePlusShareButton,
  TwitterShareButton,
  WhatsappShareButton
} = ShareButtons;

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const WhatsappIcon = generateShareIcon('whatsapp');
const GooglePlusIcon = generateShareIcon('google');

const base_url = 'https://buscandohuellas.com/search?search_id='

const ShareModal = (props) => (
  <Modal onDismiss={props.onDismiss}>
    <Box pad="large" align="center">
      <Heading tag="h3">Compartir búsqueda en:</Heading>
      <Box direction="row">
        <FacebookShareButton
          url={`${base_url}${props.search.id}`}
          title={`Buscando Huellas - ${props.search.title}`}
          description={props.search.description}
          picture={renderThumbnail(props.search.gallery, 'facebook')}
        >
          <FacebookIcon />
        </FacebookShareButton>
        <TwitterShareButton
          url={`${base_url}${props.search.id}`}
          title={props.search.title}
          hashtags={[ 'BuscandoHuellas' ]}
        >
          <TwitterIcon />
        </TwitterShareButton>
        <WhatsappShareButton
          url={`${base_url}${props.search.id}`}
          title={props.search.title}
          separator=" / "
        >
          <WhatsappIcon />
        </WhatsappShareButton>
        <GooglePlusShareButton
          url={`${base_url}${props.search.id}`}
        >
          <GooglePlusIcon />
        </GooglePlusShareButton>
      </Box>
    </Box>
  </Modal>
)

export default ShareModal