import React, { Component } from 'react'
import Joi from 'joi-browser'

//Grommet Components
import Form from 'grommet/components/Form'
import FormField from 'grommet/components/FormField'
import TextInput from 'grommet/components/TextInput'
import Button from 'grommet/components/Button'
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Spinning from 'grommet/components/icons/Spinning'
import LinkNext from 'grommet/components/icons/base/LinkNext'


class DescriptionStep extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: { value: props.email ||  '', error: null }
    }
  }

  handleChange = ({ target }) => {
    this.setState({
      [ target.name ]: {
        value: target.value,
        error: null
      }
    })
  }

  handlePrevStep = () => {
    this.props.prevHandler()
  }

  submit = () => {
    let { email } = this.state
    let schema = Joi.object().keys({
      email: Joi.string().email().required()
    })
    let formFields = {
      email: email.value
    }
    let result = Joi.validate(formFields, schema, {
      abortEarly: false,
      language: {
        any: {
          empty: '!!No puede estar vacío'
        },
        string: {
          email: '!!Ingresa un email válido'
        }
      }
    })

    if (result.error) {
      console.log(result)
      result.error.details.map((error) => {
        this.setState({
          [ error.path ]: {
            ...this.state[ error.path ],
            error: error.message
          }
        })
      })
    } else {
      this.setState({ loading: true })
      this.props.nextHandler(formFields)
    }
  }

  render() {
    const { pet_name, title, description, phone, email } = this.state

    return (
      <Box responsive={false} full="horizontal" align="center">
        <Heading tag="h4" margin="small" align="center" strong={true}>Ingresa el email en el que quieres recibir las notificaciones:</Heading>
        <Form size="large" pad="medium" className="descriptionForm">
          <FormField label="Email de Notificaciones" error={email.error}>
            <TextInput name='email' value={email.value} onDOMChange={this.handleChange} />
          </FormField>
        </Form>
        <Button
          icon={
            this.state.loading ? <Spinning className="whiteSpinning" /> : <LinkNext />
          }
          label="Continuar"
          primary={true}
          onClick={this.submit} />
      </Box>
    )
  }
}

export default DescriptionStep