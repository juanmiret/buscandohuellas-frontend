import { User } from '../services/API'
import Link from 'next/link'

//Grommet Comps
import Image from 'grommet/components/Image'
import Heading from 'grommet/components/Heading'
import Anchor from 'grommet/components/Anchor'
import Menu from 'grommet/components/Menu'
import Box from 'grommet/components/Box'
import Paragraph from 'grommet/components/Paragraph'
import Close from 'grommet/components/icons/base/Close'
import Checkmark from 'grommet/components/icons/base/Checkmark'

const ProfileInfo = ({ profile_picture, name, email, email_confirmed, openModifyEmail, resendConfirmation }) => (
  <Box align="center">
    <Image size="thumb" src={profile_picture} className="profileImage" />
    <Heading tag="h3" strong={true} margin="small">{name}</Heading>
    <Heading tag="h4"><strong>Email:</strong> {email || 'NO ESPECIFICADO'}</Heading>
    {
      email && (
        email_confirmed ?
          <Anchor className="greenAnchor" label="Email confirmado" icon={<Checkmark colorIndex="ok" />} animateIcon={false} />
          :
          <Anchor className="redAnchor" label="Email sin confirmar" icon={<Close colorIndex="critical" />} animateIcon={false} />
      )
    }
    <div style={{ height: '10px' }}></div>
    {
      email && !email_confirmed && <Anchor label="Reenviar email de confirmacion" onClick={() => resendConfirmation()} />
    }
    <Anchor label={email ? 'Cambiar mi email' : 'Agregar un email'} onClick={() => openModifyEmail()} />
    <Anchor label="Cerrar Sesión" onClick={() => User.logout()} />
  </Box>
)

export default ProfileInfo