import React from 'react'
import { User, Searches } from '../services/API'
import Head from 'next/head'

//App Comps
import Layout from '../components/Layout'
import SearchGallery from '../components/SearchGallery'
import SearchDetails from '../components/SearchDetails'
import PublicSearchHeader from '../components/PublicSearchHeader'
import ContactModal from '../components/ContactModal'
import ContactButton from '../components/ContactButton'

//Grommet Comps
import Article from 'grommet/components/Article'
import Section from 'grommet/components/Section'
import Split from 'grommet/components/Split'
import Box from 'grommet/components/Box'
import Button from 'grommet/components/Button'

//Helpers
import renderThumbnail from '../helpers/renderThumbnail'


export default class SearchPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showContactModal: false
    }
  }

  static async getInitialProps(ctx) {
    const searchDetails = await Searches.getSearchDetails(ctx.query.search_id)
    const user = await User.getCurrentUser(ctx)
    return {
      user: user,
      search: searchDetails
    }
  }


  showContactModal = () => {
    this.setState({
      showContactModal: true
    })
  }

  closeContactModal = () => {
    this.setState({
      showContactModal: false
    })
  }

  render() {
    let { user, search } = this.props
    return (
      <Layout user={user} title={search.title}>
        <Head>
          <meta property="og:url" content={`https://buscandohuellas.com/search?search_id=${search.id}`} />
          <meta property="og:title" content={"Buscando Huellas - " + search.title} />
          <meta property="og:description" content={search.description} />
          <meta property="og:type" content="article" />
          <meta property="og:image" content={renderThumbnail(search.gallery, 'facebook')} />
          <meta property="og:image:width" content="476" />
          <meta property="og:image:height" content="249" />
        </Head>
        <Split
          fixed={false}
          flex="both"
          showOnResponsive="both">
          <Box pad="small" justify="center">
            <SearchGallery gallery={search.gallery} lat={search.lat} lng={search.lng} />
          </Box>
          <Box pad="small" justify="center">
            <SearchDetails {...search} />
            <ContactButton onClick={this.showContactModal} />
          </Box>
        </Split>
        {
          this.state.showContactModal ?
            <ContactModal onDismiss={this.closeContactModal} user={search.user} doc={search} /> : null
        }
      </Layout>
    )
  }
}