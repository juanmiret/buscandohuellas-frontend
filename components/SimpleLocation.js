import React from 'react'
import { withGoogleMap, GoogleMap, Marker, Circle } from "react-google-maps"

const SimpleLocationGoogleMap = withGoogleMap(props => (
  <GoogleMap
    defaultZoom={props.withRadius ? 12 : 15}
    center={props.center}
  >
    {props.center && (
      <Marker
        position={props.center}
        draggable={false}
      />
    )}
    {
      props.center && (
        props.withRadius && (
          <Circle
            center={props.center}
            radius={props.radius * 1000}
          />
        )
      )
    }
  </GoogleMap>
));

const SimpleLocation = (props) => (
  <SimpleLocationGoogleMap
    withRadius={props.withRadius}
    radius={props.radius}
    containerElement={
      <div style={{ height: `${props.height || '150px'}`, width: '100%' }} />
    }
    mapElement={
      <div style={{ height: `100%`, width: '100%' }} />
    }
    center={props.center}
  />
)

export default SimpleLocation