import Header from 'grommet/components/Header'
import Title from 'grommet/components/Title'
import Box from 'grommet/components/Box'
import Menu from 'grommet/components/Menu'
import Actions from 'grommet/components/icons/base/Actions'
import Anchor from 'grommet/components/Anchor'
import Button from 'grommet/components/Button'

const SearchHeader = (props) => (
  <Header pad="medium" colorIndex="neutral-1-a" fixed={true} responsive={true}>
    <Title>
      {props.search.title}
    </Title>
    <Box flex={true}
      justify='end'
      direction='row'
      responsive={true}>
      <Button className="contactButton foundButton" primary={true} label="¡Se dio el reencuentro!" onClick={() => {
        props.openFoundModal(props.search.id, props.search.category)
      }} />
    </Box>
  </Header>
)

export default SearchHeader