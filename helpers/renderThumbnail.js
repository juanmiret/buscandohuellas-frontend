const renderThumbnail = (gallery, type = null) => {
  let cdn_url = gallery.cdn_url
  let thumbnail = `${cdn_url}nth/0`
  if (type == 'facebook') {
    thumbnail += "/-/scale_crop/476x249/center"
  } else {
    thumbnail += "/-/scale_crop/380x200/center"
  }
  thumbnail += "/-/quality/lighter"
  thumbnail += "/-/progressive/yes"
  thumbnail += "/"
  return thumbnail
}

export default renderThumbnail