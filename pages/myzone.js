import React from 'react'
import { User, Searches } from '../services/API'
import Geosuggest from 'react-geosuggest'
import canUseDOM from "can-use-dom"

//App Comps
import Layout from '../components/Layout'
import ProfileHeader from '../components/ProfileHeader'
import SearchTile from '../components/SearchTile'
import SearchModal from '../components/SearchModal'

//Grommet Comps
import Box from 'grommet/components/Box'
import Article from 'grommet/components/Article'
import Section from 'grommet/components/Section'
import Spinning from 'grommet/components/icons/Spinning'
import Heading from 'grommet/components/Heading'
import Header from 'grommet/components/Header'
import Paragraph from 'grommet/components/Paragraph'
import Tiles from 'grommet/components/Tiles'
import Tile from 'grommet/components/Tile'
import Anchor from 'grommet/components/Anchor'
import Tip from 'grommet/components/Tip'
import ListPlaceholder from 'grommet-addons/components/ListPlaceholder'
import Menu from 'grommet/components/Menu'
import CheckBox from 'grommet/components/CheckBox'
import Columns from 'grommet/components/Columns'

//Helpers 
import { getSpecie, getSex, getCategory } from '../helpers/displayTraductions'


export default class MyZone extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      searches: [],
      loading: true,
      filtered: [],
      orderBy: 'created_at',
      filters: {
        categories: Array.from(props.user.volunteerData ? props.user.volunteerData.categories : []),
        species: [ 'dog', 'cat', 'other' ],
        sex: [ 'male', 'female' ]
      }
    }
  }

  checkClick = (filter, value) => {
    const filterArray = this.state.filters[ filter ];
    if (filterArray.includes(value)) {
      filterArray.splice(filterArray.indexOf(value), 1)
    } else {
      filterArray.push(value)
    }
    this.setState({
      filters: {
        ...this.state.filters,
        [ filter ]: filterArray
      }
    }, () => { this.filterSearch() })
  }

  sortList = () => {
    const list = this.state.filtered;
    if (this.state.orderBy == 'distance') {
      list.sort(function (a, b) {
        return a.dist - b.dist
      })
    } else {
      list.sort(function (a, b) {
        let c = new Date(a.doc.created_at);
        let d = new Date(b.doc.created_at);
        return d.getTime() - c.getTime();
      });
    }
    this.setState({
      filtered: list
    })
  }

  filterSearch = () => {
    let filteredArray = Array.from(this.state.searches)
    filteredArray.map(
      (search) => {
        let { category, specie, sex } = search.doc
        if (this.state.filters.categories.includes(category) &&
          this.state.filters.species.includes(specie) &&
          this.state.filters.sex.includes(sex)) {
          search.doc.hidden = false
        } else {
          search.doc.hidden = true
        }
      }
    )
    this.setState({
      filtered: filteredArray
    })
  }

  orderBy = (val) => {
    this.setState({
      orderBy: val
    }, () => { this.sortList() })
  }

  static async getInitialProps(ctx) {
    const user = await User.getCurrentUser(ctx)
    return {
      user: user
    }
  }

  async getList() {
    this.setState({
      loading: true
    })
    let results = await Searches.getVolunteerSearches()
    this.setState({
      searches: results,
      loading: false,
      filtered: results
    }, () => { this.sortList() })
  }

  dismissModal = () => {
    this.setState({ searchModal: null })
  }

  renderModal = () => {
    if (this.state.searchModal) {
      return (
        <SearchModal {...this.state.searchModal}
          searchModalIndex={this.state.searchModalIndex}
          onDismiss={() => this.dismissModal()}
          noDiscard
        />
      )
    }
  }

  componentDidMount() {
    this.getList();
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }


  render() {
    let { user } = this.props
    let { filters } = this.state
    return (
      <Layout user={user} pathname={this.props.url.pathname} title="Mi Zona">
        {this.renderModal()}
        <Article full={true} colorIndex="light-2">
          <ProfileHeader title="Mi Zona" />
          <Heading style={{ margin: '20px 0' }} align="center" tag="h4" strong={true}>Resultados de publicaciones para tu zona:</Heading>
          <Section>
            <Box direction="row" justify="center" align="center">
              Filtros:
              <Menu
                responsive={false}
                label='Categorias'
                closeOnClick={false} >
                {
                  user.volunteerData && user.volunteerData.categories.map((e) => {
                    return (
                      <CheckBox className="checkCat" label={getCategory(e)}
                        defaultChecked={filters.categories.includes(e)}
                        onChange={() => { this.checkClick('categories', e) }} />
                    )
                  })
                }
              </Menu>

              <Menu
                responsive={false}
                label='Especie'
                closeOnClick={false}>
                <CheckBox className="checkCat" label="Perro" key="dog"
                  defaultChecked={filters.species.includes('dog')}
                  onChange={() => { this.checkClick('species', 'dog') }} />

                <CheckBox className="checkCat" label="Gato" key="cat"
                  defaultChecked={filters.species.includes('cat')}
                  onChange={() => { this.checkClick('species', 'cat') }} />

                <CheckBox className="checkCat" label="Otro" key="other"
                  defaultChecked={filters.species.includes('other')}
                  onChange={() => { this.checkClick('species', 'other') }} />
              </Menu>
              <Menu
                responsive={false}
                label='Sexo'
                closeOnClick={false}>
                <CheckBox className="checkCat" label="Macho" key="male"
                  defaultChecked={filters.sex.includes('male')}
                  onChange={() => { this.checkClick('sex', 'male') }} />
                <CheckBox className="checkCat" label="Hembra" key="female"
                  defaultChecked={filters.sex.includes('female')}
                  onChange={() => { this.checkClick('sex', 'female') }} />
              </Menu>
              <Menu responsive={false}
                label='Ordenar por:'>
                <Anchor onClick={() => { this.orderBy('created_at') }}>
                  Más recientes primero
                  </Anchor>
                <Anchor onClick={() => { this.orderBy('distance') }}>
                  Más cercanas primero
                  </Anchor>
              </Menu>
            </Box>
          </Section>
          <Section pad="small" align="center" justify="center">
            {
              this.state.loading ? <Spinning size="large" /> :
                <div>
                  <Tiles flush={false} justify="start" fill={true}>
                    <ListPlaceholder
                      emptyMessage={"No hay resultados de esta categoria para tu zona."}
                      unfilteredTotal={this.state.searches.length}
                      filteredTotal={this.state.filtered.length} />
                    {
                      this.state.filtered.map((search, index) => {
                        return (
                          <SearchTile key={index} search={search.doc} dist={search.dist} withMeta noAnimation>
                            <Header size='small'>
                              <Anchor primary={true} label="Ver Detalles" onClick={() => {
                                this.setState({ searchModal: search, searchModalIndex: index })
                              }} />
                            </Header>
                          </SearchTile>
                        )
                      })
                    }
                  </Tiles>
                </div>
            }
            {
              !user.volunteerData &&
              <Paragraph>Tuvimos un problema con la funcion voluntarios en algunos usuarios,
              si estas viendo este mensaje, por favor vuelve a cargar los datos de Voluntario
              desde el panel Mi Perfil.
              ¡Te pedimos disculpas por los inconvenientes!</Paragraph>
            }
          </Section>
        </Article>
      </Layout>
    )
  }
}
