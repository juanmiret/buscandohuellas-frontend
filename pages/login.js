import React, { Component } from 'react'
import LoginButtons from '../components/LoginButtons'
import HomeLayout from '../components/HomeLayout'
import { User } from '../services/API'
import redirect from '../lib/redirect'
import Link from 'next/link'
import Joi from 'joi-browser'

//Grommet Components
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Paragraph from 'grommet/components/Paragraph'
import Image from 'grommet/components/Image'
import Spinning from 'grommet/components/icons/Spinning'
import Form from 'grommet/components/Form'
import FormField from 'grommet/components/FormField'
import TextInput from 'grommet/components/TextInput'
import Button from 'grommet/components/Button'


export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      loginError: null,
      email: { value: '', error: null },
      password: { value: '', error: null }
    }
  }

  static async getInitialProps(ctx) {
    const user = await User.getCurrentUser(ctx)
    const redirectPath = ctx.query.redirect
    const email_confirmed = ctx.query.email_confirmed
    if (user) {
      redirect(ctx, '/userpanel')
      return {}
    } else {
      return {
        redirectPath: redirectPath,
        email_confirmed: email_confirmed
      }
    }
  }

  handleChange = ({ target }) => {
    this.setState({
      [ target.name ]: {
        value: target.value,
        error: null
      }
    })
  }

  startLoading = () => {
    this.setState({
      loading: true
    })
  }

  submit = () => {
    let { email, password } = this.state
    let schema = Joi.object().keys({
      email: Joi.string().email().required(),
      password: Joi.string().min(6).required()
    })
    let formFields = {
      email: email.value,
      password: password.value
    }
    let result = Joi.validate(formFields, schema, {
      abortEarly: false,
      language: {
        any: {
          empty: '!!No puede estar vacío'
        },
        string: {
          email: '!!Ingresa un email válido',
          min: '!!Minimo 6 caracteres.'
        }
      }
    })

    if (result.error) {
      result.error.details.map((error) => {
        this.setState({
          [ error.path ]: {
            ...this.state[ error.path ],
            error: error.message
          }
        })
      })
    } else {
      this.startLogin(formFields.email, formFields.password)
    }
  }

  async startLogin(email, password) {
    this.setState({ loading: true, loginError: null })
    const response = await User.loginWithEmail(email, password, this.props.redirectPath)
    if (response.error) {
      this.setState({
        loading: false,
        loginError: response.message
      })
    }
  }

  render() {
    let { email, password } = this.state
    return (
      <HomeLayout>
        <Box full={true} justify="center" align="center" pad="small">
          <Image src="/static/buscandohuellas_logo_azul.png" size="small" />
          <Heading tag="h3" strong align="center" > Inicia sesión para continuar: </Heading>
          <LoginButtons redirectPath={this.props.redirectPath} startLoading={this.startLoading} />
          <Heading tag="h3" align="center"> -- o -- </Heading>
          {this.state.loading && <Spinning size="medium" />}
          {this.state.loginError && <Paragraph>*{this.state.loginError}</Paragraph>}
          {this.props.email_confirmed && <Paragraph>Tu email fue confirmado con éxito, ya puedes Iniciar sesión con tu email y contraseña</Paragraph>}
          <Box alignContent="center" justify="center">
            <Form size="small" compact>
              <FormField label="Email" error={email.error}>
                <TextInput name="email" value={email.value} onDOMChange={this.handleChange} />
              </FormField>
              <FormField label="Contraseña" error={password.error}>
                <TextInput type="password" name="password" value={password.value} onDOMChange={this.handleChange} />
              </FormField>
            </Form>
            <Button primary label="Iniciar Sesión" onClick={this.submit} />
            <Heading tag="h3" align="center"> -- o -- </Heading>
            <Link href="/sign_up">
              <Button primary label="Registrarme" />
            </Link>
          </Box>
          <Paragraph size="small" align="center">
            Al iniciar sesión aceptas nuestros <Link href="/terms">Terminos y Condiciones</Link> de uso.
          </Paragraph>
        </Box>
      </HomeLayout>
    )
  }
}