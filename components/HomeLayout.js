import React, { Component } from 'react'
import Head from 'next/head'
import UserSidebar from './UserSidebar'
import Router from 'next/router'

import GaWrapper from '../components/GaWrapper'

import App from 'grommet/components/App'
import Anchor from 'grommet/components/Anchor'
import Image from 'grommet/components/Image'
import Responsive from 'grommet/utils/Responsive'
import Box from 'grommet/components/Box'
import Menu from 'grommet/components/icons/base/Menu'

import stylesheet from '../styles/index.scss'

class HomeLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    this._responsive = Responsive.start(this._onResponsive);
    Router.prefetch('/userpanel')
    Router.prefetch('/usersearches')
    Router.prefetch('/addsearch')
  }

  componentWillUnmount() {
    this._responsive.stop();
  }

  _onResponsive = (small) => {
    this.setState({ small });
  }

  openSidebar = () => {
    this.setState({
      priority: 'left'
    })
  }

  closeSidebar = () => {
    this.setState({
      priority: 'right'
    })
  }

  renderMobileHeader() {
    if (this.state.small) {
      return (
        <Header colorIndex="neutral-1" pad="small" justify="between">
          <Image src="/static/bh_logo.png" size="small" />
          <Box>
            <Anchor icon={<Menu />} onClick={this.openSidebar} />
          </Box>
        </Header>
      )
    }
  }

  render() {
    return (
      <div>
        <Head>
          <title>Buscando Huellas - El lugar para encontrar mascotas perdidas y en adopción.</title>
          <meta property="og:url" content="https://buscandohuellas.com" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="Buscando Huellas" />
          <meta property="og:description" content="La plataforma más efectiva del mundo para mascotas perdidas, en adopción o en estado de emergencia." />
          <meta property="og:image" content="https://buscandohuellas.com/static/buscandohuellas_logo_azul.png" />
        </Head>
        <App centered={false}>
          {this.props.children}
        </App>
      </div>
    )
  }
}

export default GaWrapper(HomeLayout)