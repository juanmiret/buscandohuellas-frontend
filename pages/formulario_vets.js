import React, { Component } from 'react'
import Joi from 'joi-browser'

import { Vets } from '../services/API'

//Grommet Components
import Button from 'grommet/components/Button'
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Spinning from 'grommet/components/icons/Spinning'
import LinkNext from 'grommet/components/icons/base/LinkNext'
import Columns from 'grommet/components/Columns'
import Image from 'grommet/components/Image'
import Select from 'grommet/components/Select';


const FormInput = (props) => {
  return (
    <div className={`form-input form-input-name-row ${props.error && "form-invalid-data"}`}>
      <label>
        <span>{props.label}</span>
        {
          props.hint && <p>{props.hint}</p>
        }
        {
          props.type === 'textarea' ?
            <textarea rows="8" name={props.name} value={props.value} onChange={props.onChange} />
            :
            <input name={props.name} value={props.value} onChange={props.onChange} />
        }

      </label>
      <span className="form-invalid-data-info">{props.error}</span>
    </div>
  )
}

const SelectInput = (props) => {
  return (
    <div className={`form-input form-input-name-row ${props.error && "form-invalid-data"}`}>
      <label>
        <span>{props.label}</span>
        <Select name={props.name} options={props.options} value={props.value} onChange={props.onChange} />
      </label>
      <span className="form-invalid-data-info">{props.error}</span>
    </div>
  )
}


class FormularioVets extends Component {
  constructor(props) {
    super(props)
    this.state = {
      business_name: { value: '', error: null },
      contact_name: { value: '', error: null },
      address: { value: '', error: null },
      city: { value: '', error: null },
      state: { value: 'Buenos Aires', error: null },
      country: { value: 'Argentina', error: null },
      zipcode: { value: '', error: null },
      phone: { value: '', error: null },
      email: { value: '', error: null },
      razon_social: { value: '', error: null },
      cuit: { value: '', error: null },
      description: { value: '', error: null },
      gallery: {}
    }
  }

  componentDidMount() {
    var that = this
    var multiWidget = uploadcare.MultipleWidget('[role=uploadcare-uploader][data-multiple]');
    multiWidget.onUploadComplete((gallery) => {
      // Handle uploaded file info.
      that.setState({
        gallery: {
          cdn_url: gallery.cdnUrl,
          uuid: gallery.uuid,
        }
      })
    });
  }

  handleChange = ({ target }) => {
    this.setState({
      [ target.name ]: {
        value: target.value,
        error: null
      }
    })
  }

  handleSelect = ({ target, option }) => {
    this.setState({
      [ target.name ]: {
        value: option,
        error: null
      }
    })
  }

  handlePrevStep = () => {
    this.props.prevHandler()
  }

  submit = () => {
    let { razon_social, gallery, business_name, contact_name, description, phone, email, address, city, state, country, zipcode, cuit } = this.state
    let schema = Joi.object().keys({
      business_name: Joi.string().required(),
      contact_name: Joi.string().required(),
      address: Joi.string().required(),
      city: Joi.string().required(),
      state: Joi.string().required(),
      country: Joi.string().required(),
      zipcode: Joi.string().required(),
      razon_social: Joi.string().required(),
      cuit: Joi.string().required(),
      phone: Joi.string().required(),
      email: Joi.string().required(),
      description: Joi.string().required(),
      gallery: Joi.object()
    })
    let formFields = {
      business_name: business_name.value,
      contact_name: contact_name.value,
      address: address.value,
      phone: phone.value,
      email: email.value,
      city: city.value,
      state: state.value,
      country: country.value,
      zipcode: zipcode.value,
      razon_social: razon_social.value,
      cuit: cuit.value,
      description: description.value,
      gallery: gallery
    }
    let result = Joi.validate(formFields, schema, {
      abortEarly: false,
      language: {
        any: {
          empty: '!!No puede estar vacío'
        },
        string: {
          email: '!!Ingresa un email válido'
        }
      }
    })

    if (result.error) {
      console.log(result)
      result.error.details.map((error) => {
        this.setState({
          [ error.path ]: {
            ...this.state[ error.path ],
            error: error.message
          }
        })
      })
    } else {
      this.setState({ loading: true })
      this.submitVet(formFields)
    }
  }

  async submitVet(formData) {
    const res = await Vets.newVet(formData)
    if (res.error) {
      console.log(res.error)
    } else if (res.redirect) {
      window.location.href = res.redirect
    }
  }

  render() {
    let { razon_social, business_name, contact_name, description, phone, email, address, city, state, country, zipcode, cuit } = this.state
    let countryOptions = [ 'Argentina' ]
    let stateOptions = [ 'Buenos Aires', 'Catamarca', 'Chaco', 'Chubut', 'Cordoba', 'Corrientes', 'Entre Rios', 'Formosa', 'Jujuy', 'La Pampa', 'La Rioja', 'Mendoza', 'Misiones', 'Neuquen', 'Rio Negro', 'Salta', 'San Juan', 'San Luis', 'Santa Cruz', 'Santa Fe', 'Santiago del Estero', 'Tierra del Fuego', 'Tucuman' ]
    return (
      <form className="form-validation" method="post">
        <Image src="/static/buscandohuellas_logo_azul.png" size="medium" />
        <div className="form-title-row">
          <h1>Formulario para Veterinarias</h1>
        </div>
        <Columns maxCount={2} justify="between">
          <FormInput label="Nombre del Comercio" name="business_name" value={business_name.value} error={business_name.error} onChange={this.handleChange} />
          <FormInput label="Nombre del Contacto" name="contact_name" value={contact_name.value} error={contact_name.error} onChange={this.handleChange} />
          <FormInput label="Teléfono de Contacto" name="phone" value={phone.value} error={phone.error} onChange={this.handleChange} />
          <FormInput label="Email" name="email" value={email.value} error={email.error} onChange={this.handleChange} />
          <FormInput label="Razón Social" name="razon_social" value={razon_social.value} error={razon_social.error} onChange={this.handleChange} />
          <FormInput label="CUIT" name="cuit" value={cuit.value} error={cuit.error} onChange={this.handleChange} />
          <SelectInput label="País" options={countryOptions} name="country" value={country.value} error={country.error} onChange={this.handleSelect} />
          <SelectInput label="Provincia" options={stateOptions} name="state" value={state.value} error={state.error} onChange={this.handleSelect} />
          <FormInput label="Ciudad" name="city" value={city.value} error={city.error} onChange={this.handleChange} />
          <FormInput label="Direccion" name="address" value={address.value} error={address.error} onChange={this.handleChange} />
          <FormInput label="Código Postal" name="zipcode" value={zipcode.value} error={zipcode.error} onChange={this.handleChange} />
        </Columns>
        <FormInput type="textarea" label="Descripcion Pública" hint="Esta es la descripción de tu negocio que va a aparecer en nuestra plataforma." name="description" value={description.value} error={description.error} onChange={this.handleChange} />
        <div className="form-input form-input-name-row">
          <label>
            <span>
              Sube fotos de tu Local/Logo/Marca etc... Estas fotos serán mostradas en nuestra plataforma.
            </span>
          </label>
          <input type="hidden" role="uploadcare-uploader"
            data-images-only="true"
            data-crop="true"
            data-multiple="true"
            data-multiple-max={4}
            data-image-shrink="1400x1400"
            data-tabs="file url facebook gdrive gphotos dropbox instagram"
            data-locale="es"
          />
        </div>
        <Button primary label="Confirmar" onClick={this.submit} icon={this.state.loading ? <Spinning className="spinningWhite" /> : <LinkNext />} />
      </form>
    )
  }
}

export default FormularioVets