import React from 'react'
import { User, Searches } from '../services/API'
import Geosuggest from 'react-geosuggest'
import canUseDOM from "can-use-dom";


//App Comps
import Layout from '../components/Layout'
import ProfileHeader from '../components/ProfileHeader'
import SearchTile from '../components/SearchTile'
import SearchModal from '../components/SearchModal'

//Grommet Comps
import Box from 'grommet/components/Box'
import Article from 'grommet/components/Article'
import Section from 'grommet/components/Section'
import Spinning from 'grommet/components/icons/Spinning'
import Heading from 'grommet/components/Heading'
import Header from 'grommet/components/Header'
import Tiles from 'grommet/components/Tiles'
import Tile from 'grommet/components/Tile'
import Anchor from 'grommet/components/Anchor'
import Tip from 'grommet/components/Tip'
import ListPlaceholder from 'grommet-addons/components/ListPlaceholder'


export default class SearchAdoptions extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      address: '',
      center: {},
      query: {},
      adoptions: [],
      loadingLocation: true,
      loading: false
    }
  }

  static async getInitialProps(ctx) {
    const user = await User.getCurrentUser(ctx)
    return {
      user: user
    }
  }

  onSuggestSelect = (suggest) => {
    this.setState({
      center: suggest.location,
      address: suggest.description
    })
    this.getList()
  }

  componentDidMount() {
    this.geocoder = new google.maps.Geocoder
    geolocation.getCurrentPosition((position) => {
      if (this.isUnmounted) {
        return;
      }
      let center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      }
      this.geocoder.geocode({ location: center }, (results, status) => {
        if (status === 'OK') {
          if (results[ 0 ]) {
            this.setState({ address: results[ 0 ].formatted_address })
            this._geoSuggest.update(results[ 0 ].formatted_address);
          }
        }
      })
      this.setState({
        center: center,
        loadingLocation: false,
        content: `Location found using HTML5.`,
      });
      this.getList()
    }, (reason) => {
      if (this.isUnmounted) {
        return;
      }
      this.setState({
        center: null,
        loadingLocation: false,
        content: `Error: The Geolocation service failed (${reason}).`,
      });
    });
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }


  async getList() {
    this.setState({
      loading: true
    })
    let results = await Searches.getAdoptions(this.state.center, this.state.query)
    this.setState({
      adoptions: results,
      loading: false
    })
  }

  dismissModal = () => {
    this.setState({ searchModal: null })
  }

  renderModal = () => {
    if (this.state.searchModal) {
      return (
        <SearchModal {...this.state.searchModal}
          searchModalIndex={this.state.searchModalIndex}
          onDismiss={() => this.dismissModal()} />
      )
    }
  }

  render() {
    return (
      <Layout user={this.props.user} pathname={this.props.url.pathname} title="Adopciones">
        {this.renderModal()}
        <Article full={true} colorIndex="light-2">
          <ProfileHeader title="Adopciones" />
          {
            this.state.loadingLocation ?
              <Section flex="grow" direction="column" justify="center" align="center">
                <Spinning size="large" />
              </Section>
              :
              <Section pad="small" align="center" justify="center">
                <Geosuggest
                  ref={el => this._geoSuggest = el}
                  placeholder="Ingresa una dirección y selecciona una opcion de la lista..."
                  onSuggestSelect={this.onSuggestSelect}
                />
                {
                  this.state.loading ? <Spinning size="large" /> :
                    <div>
                      <Heading style={{ margin: '20px 0' }} align="center" tag="h4" strong={true}>Mascotas en adopcion cercanas a tu ubicación:</Heading>
                      <Tiles flush={false} justify="start" fill={true}>
                        <ListPlaceholder
                          emptyMessage="No hay resultados para esta ubicacion."
                          unfilteredTotal={this.state.adoptions.length}
                          filteredTotal={this.state.adoptions.length} />
                        {this.state.adoptions.map((search, index) => {
                          return (
                            <SearchTile key={index} search={search.doc} dist={search.dist} withMeta>
                              <Header size='small'>
                                <Anchor primary={true} label="Ver Detalles" onClick={() => {
                                  this.setState({ searchModal: search, searchModalIndex: index })
                                }} />
                              </Header>
                            </SearchTile>
                          )
                        })}
                      </Tiles>
                    </div>
                }
              </Section>
          }
        </Article>
      </Layout>
    )
  }
}

const geolocation = (
  canUseDOM && navigator.geolocation ?
    navigator.geolocation :
    ({
      getCurrentPosition(success, failure) {
        failure(`Your browser doesn't support geolocation.`);
      },
    })
);