import { FacebookLogin } from 'react-facebook-login-component'
import { GoogleLogin } from 'react-google-login-component';
import { User } from '../services/API'

const FB_ID = FACEBOOK_ID
const G_ID = GOOGLE_ID

const LoginButtons = (props) => (
  <div>
    <FacebookLogin
      socialId={FB_ID}
      language="es_LA"
      scope="public_profile,email"
      responseHandler={(res) => {
        props.startLoading()
        User.responseFacebook(res, props.redirectPath)
      }}
      xfbml={true}
      fields="id,email,name"
      version="v2.9"
      className="loginBtn loginBtn--facebook"
      buttonText="Iniciar Sesión con Facebook" />
    <GoogleLogin
      socialId={G_ID}
      className="loginBtn loginBtn--google"
      scope="profile https://www.googleapis.com/auth/plus.profile.emails.read"
      responseHandler={(res) => {
        props.startLoading()
        User.responseGoogle(res, props.redirectPath)
      }}
      buttonText="Iniciar Sesión con Google" />
  </div>
)

export default LoginButtons