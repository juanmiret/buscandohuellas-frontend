import Link from 'next/link'
//Grommet Comps
import Heading from 'grommet/components/Heading'
import Paragraph from 'grommet/components/Paragraph'
import Location from 'grommet/components/icons/base/Location'
import Columns from 'grommet/components/Columns'
import Box from 'grommet/components/Box'
import Anchor from 'grommet/components/Anchor'
//App Comps
import SearchLabels from '../components/SearchLabels'
import SimpleLocation from '../components/SimpleLocation'
import ShareModalButton from '../components/ShareModalButton'

import moment from 'moment'
moment.locale('es')

const renderTitle = (search) => {
  return search.pet_name ?
    `${search.pet_name} - ${search.title}` :
    `${search.title}`
}

const SearchDetails = (props) => (
  <Box className="searchDetailsComp">
    <Heading tag="h2" strong={true}>{renderTitle(props)}</Heading>
    <div className="searchCreatedAt">Publicado el día {moment(props.created_at).format('LLL')}</div>
    <SearchLabels search={{ ...props }} />
    {
      props.found &&
      <div className="foundLabel" style={{ margin: '20px auto' }}>¡Esta publicacion ya fue resuelta!</div>
    }
    <div className="searchAddress"><Location />{props.address}</div>
    <Box direction="row">
      <ShareModalButton search={props} />
      <Link href={`/search?search_id=${props.id}`}>
        <Anchor style={{ alignSelf: 'center', margin: '10px' }} label="Ir a la publicación" />
      </Link>
    </Box>
    {
      [ 'lost', 'found' ].includes(props.category) &&
      <Paragraph margin="small">
        <strong>Fecha que se perdio/encontro: </strong>
        {props.date ? moment(props.date, "DD/MM/YYYY").format('LL') : 'No especificado'}
      </Paragraph>
    }
    <Paragraph margin="small">{props.description}</Paragraph>
    <SimpleLocation center={{ lat: props.lat, lng: props.lng }} />
  </Box>
)

export default SearchDetails